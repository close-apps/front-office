/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

import { HttpHeaders } from "@angular/common/http";

export const environment = {
  production: false,

  httpOptions: {
    headers: new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: "my-auth-token",
    }),
  },
  //BASE_URL: "https://close-apps-back-end.herokuapp.com/",
  //BASE_URL: "http://161.97.155.52:9000/",
  BASE_URL: "http://localhost:9000/",

  //BASE_URL: "https://spasuce.com/api/projet-spasuce-stg-0.1/",
  //BASE_URL:"https://spasuce.com/api/projet-spasuce-0.1/",
  //BASE_URL:"https://207.180.196.73:8080/api/projet-spasuce-0.1/",
  //BASE_URL: "http://localhost:8080/",
  //BASE_URL:"http://192.168.1.104:8080/",
  //BASE_URL: "http://192.168.1.66:808O/",
  //BASE_URL: "http://localhost:8080/",
  //BASE_URL: "http://localhost:8081/",
  //BASE_URL: "http://192.168.1.100:8080/",

  //BASE_URL:"https://spasuce-api.herokuapp.com/",

  user: "user",
  souscriptionProgrammeFideliteCarte: "souscriptionProgrammeFideliteCarte",
  souscriptionProgrammeFidelite: "souscriptionProgrammeFidelite",
  historiqueCarteProgrammeFideliteTampon:
    "historiqueCarteProgrammeFideliteTampon",
  historiqueCarteProgrammeFideliteCarte:
    "historiqueCarteProgrammeFideliteCarte",
  pointDeVenteProgrammeFideliteCarte: "pointDeVenteProgrammeFideliteCarte",
  pointDeVenteProgrammeFideliteTampon: "pointDeVenteProgrammeFideliteTampon",
  delitProgrammeFidelite: "delitProgrammeFidelite",

  //userEnseigne: "userEnseigne",
  enseigneId: "enseigneId",
  pointDeVenteId: "pointDeVenteId",
  fichier: "fichier",
  CLASSE_A: "CLASSE_A",
  CLASSE_B: "CLASSE_B",
  CLASSE_C: "CLASSE_C",
  enseigne: "enseigne",
  appelDoffres: "appelDoffres",
  appelDoffresRestreint: "appelDoffresRestreint",
  offre: "offre",
  role: "role",
  commune: "commune",
  typeAction: "typeAction",
  secteurActivite: "secteurActivite",
  uniteTemps: "uniteTemps",
  pointDeVente: "pointDeVente",
  programmeFideliteTampon: "programmeFideliteTampon",
  programmeFideliteCarte: "programmeFideliteCarte",
  carte: "carte",

  partenaires: "partenaires",
  //ypeRegleSecuriteTampon: "typeRegleSecuriteTampon",
  //typeRegleSecuriteCarte: "typeRegleSecuriteCarte",
  secteurDactiviteAppelDoffres: "secteurDactiviteAppelDoffres",
  secteurDactiviteEntreprise: "secteurDactiviteEntreprise",
  valeurCritereOffre: "valeurCritereOffre",
  contenuStatique: "contenuStatique",
  objetNousContacter: "objetNousContacter",
  souscription: "souscription",
  datasOffresNotFound: "Aucune offre trouvée. Merci d'en creer !!!",

  allPDFCartePourCePDV:
    "Tous les programme à points sont disponible dans ce point de vente",
  allPDFTamponPourCePDV:
    "Tous les programme à tampons sont disponible dans ce point de vente",

  allPDVPourCePDFCarte:
    "Ce programme à points est disponible dans tous les points de vente",
  allPDVPourCePDFTampon:
    "Ce programme à tampons est disponible dans tous les points de vente",

  merciDeVousConnectez: "Merci de vous connectez !!!",
  aucunUserPourCePDV: "Aucun gérant pour ce PDV. Merci d'ajouter un gérant !!!",
  aucunGPSPourCePDV:
    "Aucune coordonnées GPS pour ce PDV. Merci d'ajouter les coordonnées GPS de ce PDV !!!",
  donneeNonRenseigne: "Donnee non renseignée",
  // les typeAppelDoffresCode
  RESTREINT: "RESTREINT",
  GENERAL: "GENERAL",

  // les etats code

  BROUILLON: "BROUILLON",
  ENTRANT: "ENTRANT",
  TERMINER: "TERMINER",

  FOURNISSEUR: "FOURNISSEUR",
  ADMINISTRATEUR: "ADMINISTRATEUR",
  UTILISATEUR_ENSEIGNE: "UTILISATEUR ENSEIGNE",
  UTILISATEUR_MOBILE: "UTILISATEUR MOBILE",
  CLIENTE: "CLIENTE",
  MIXTE: "MIXTE",

  NOS_CONDITIONS: "NOS_CONDITIONS",
  COMMENT_CA_MARCHE: "COMMENT_CA_MARCHE",
  QUI_SOMMES_NOUS: "QUI_SOMMES_NOUS",
  SERVICES_FONCTIONNEMENT: "SERVICES_FONCTIONNEMENT",
  FONCTIONNEMENT: "FONCTIONNEMENT",
  PRESENTATION: "PRESENTATION",
  REALISATIONS: "REALISATIONS",

  // token
  token: "token",
  email: "email",
  id: "id",

  // navigation
  ordre: "element",

  ACCUEIL: "ACCUEIL",
  Pro: "Pro",
  Application: "Application",
  VOIR: "VOIR",
  SOUMETTRE: "SOUMETTRE",
  CREER: "CREER",
  ENREGISTRER: "ENREGISTRER",
  NOUVEAU: "NOUVEAU",
  VISUALISER: "VISUALISER",
  EDITER: "EDITER",
  MODIFIER: "MODIFIER",
  RETIRER: "RETIRER",
  ANNULER: "ANNULER",
  //TEMPS_TOAST : 3000, // 3s
  TEMPS_TOAST: 5000, // 3s
  TEMPS_NOTIFICATIONS: 30000, // 30s

  // message de retour
  AUCUNE_DUREE: "AUCUNE DUREE SAISIE",

  // notifications
  primary: "primary",
  success: "success",
  warning: "warning",
  danger: "danger",
  info: "info",
  //**********************

  // critereFiltre
  TOUS: "TOUS",
  SOUMISSIONNER: "SOUMISSIONNER",
  SELECTIONNER: "SELECTIONNER",
  NON_SOUMISSIONNER: "NON SOUMISSIONNER",
  LISTES_CRITERES_FILTRES: [
    "TOUS",
    "SOUMISSIONNER",
    "NON SOUMISSIONNER",
    "SELECTIONNER",
  ],

  isAuth: "isAuth",
  authenticatedUser: "authenticatedUser",
  login: "login",
  urlLogo: "urlLogo",
  defaultUrlLogo: "assets/images/logo_fond_bleu.png",
  color: "color",
  defaultColor: "#000537",
  nom: "nom",
  isAdminEnseigne: "isAdminEnseigne",
  NBRE_MAXI_USER_PAR_ENTREPRISE: 5,

  // CRUD
  flasherCarte: "/flasherCarte",
  flasherTampon: "/flasherTampon",
  create: "/create",
  update: "/update",
  getBase64Files: "/getBase64Files",
  getByCriteria: "/getByCriteria",
  getBasicStat: "/getBasicStat",
  getStatistiques: "/getStatistiques",
  delete: "/delete",
  forceDelete: "/forceDelete",
  checkEmailWithTokenIsValid: "/checkEmailWithTokenIsValid",
  updateSecteurDactivite: "/updateSecteurDactivite",

  // les routes
  pages: "/pages/",
  authentification: "/authentification/",

  CChangerPassword: "changer-password",
  CTokenInvalide: "token-invalide",
  CLogin: "login",
  CDemandeInscription: "demande-inscription",
  CInscription: "inscription",
  CEditerPDFCarteEnseigne: "editer-pdfcarte-enseigne",
  CPDFCarteEnseigne: "pdfcarte-enseigne",
  CEditerPDFTamponEnseigne: "editer-pdftampon-enseigne",
  CPDFTamponEnseigne: "pdftampon-enseigne",
  CPDVEnseigne: "pdv-enseigne",
  CEditerPDVEnseigne: "editer-pdv-enseigne",

  CEncours: "**",

  CMajProfilEnseigne: "maj-profil-enseigne",
  CMajProfilUser: "maj-profil-user",
  CLogoutUser: "logout-user",
  CMesClients: "mes-clients",
  CScannerQrCode: "scanner-qr-code",
  CScannerQrCodeTampon: "scanner-tampon",
  CDetailsClients: "details-client",
  CDetailsHistoriqueClient: "details-historique-client",
  CNousContacter: "nous-contacter",
  CConfidentialite: "confidentialite",
  CPro: "pro",

  CPasswordOublier: "password-oublier",

  //AUTRES
  connexion: "/connexion",
  changerPassowrd: "/changerPassowrd",
  passwordOublier: "/passwordOublier",
  changerPasswordOublier: "/changerPasswordOublier",
  updateCustom: "/updateCustom",
  choisirOffre: "/choisirOffre",
  getNotifications: "/getNotifications",
  updateNotifications: "/updateNotifications",
  getByCriteriaCustom: "/getByCriteriaCustom",
  updateContenuNotificationDispo: "/updateContenuNotificationDispo",

  // les pages

  pageAccueil: "/pages/accueil",
  pageTableauDeBord: "/pages/tableau-de-bord",
  pageLogin: "/authentification/login",
  pagePasswordOublier: "/authentification/password-oublier",

  //size : 1 ,
  size: 5,

  erreurDeConnexion: "Erreur de connexion. Veuillez reessayer !!!",
  erreurDeConnexionOuDataIncorrect:
    "Erreur de connexion et/ou code incorrect. Veuillez reessayer !!!",
  messageAttenteApresScan: "Votre scan a été effectué. Traitement en cours !!!",
  // menu utilisateur
  monProfil: "Profil enseigne",
  profilUser: "Mon profil",
  changerPassword: "Changer mon mot de passe",
  tableauDeBord: "Tableau de bord",
  confidentialite: "Confidentialité",
  autres: "Autres",
  deconnexion: "Déconnexion",
  seConnecter: "Se connecter",
  nousRejoindre: "Nous rejoindre",
  mesClients: "Mes clients",
  scannerCarte: "Scanner carte",
  scannerTampon: "Scanner tampon",
  mesAbonnees: "Mes abonnées",
  // mesPDF: "Mes programmes de fidélité",
  // mesPDFTampon: "Programmes de fidélité tampon",
  // mesPDFCarte: "Programmes de fidélité carte",
  mesPDFTampon: "Programmes à tampons",
  mesPDFCarte: "Programmes à points",
  //mesPDV: "Mes points de vente",
  mesPDV: "Points de vente",
  //nousContacter: "NOUS CONTACTER",
  nousContacter: "Nous Contacter",
  servicesEtFaq: "SERVICE CLIENT / FAQ",
  afficherScannerQrCode: "Scanner QrCode",
  cacherScannerQrCode: "Fermer Scanneur",
  scannerManuellemnt: "scanner-manuellement",
  scannerManuellemntParProgramme: "scanner-manuellement-par-programme",
  CODEDEBITER: "DEBITER",
  CODECREDITER: "CREDITER",
  datasDebitCredit: [
    { code: "DEBITER", libelle: "DEBIT" },
    { code: "CREDITER", libelle: "CREDIT" },
  ],
};
