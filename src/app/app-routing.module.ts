import { ExtraOptions, RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import {
  NbAuthComponent,
  NbLoginComponent,
  NbLogoutComponent,
  NbRegisterComponent,
  NbRequestPasswordComponent,
  NbResetPasswordComponent,
} from "@nebular/auth";
import { AuthentificationComponent } from "./authentification/authentification.component";
import { LoginComponent } from "./authentification/login/login.component";
import { PasswordOublierComponent } from "./authentification/password-oublier/password-oublier.component";
import { ChangerPasswordComponent } from "./authentification/changer-password/changer-password.component";
import { TestComponent } from "./test/test.component";
import { ResetComponent } from "./reset/reset.component";

const routes: Routes = [
  {
    path: "pages",
    //path: "",
    //loadChildren: () => import('app/pages/pages.module')
    loadChildren: () =>
      import("./pages/pages.module").then((m) => m.PagesModule),
  },
  {
    path: "authentification",
    loadChildren: () =>
      import("./authentification/authentification.module").then(
        (m) => m.AuthentificationModule
      ),
  },
  // {
  //   path: "authentification",
  //   component: AuthentificationComponent,
  //   children: [
  //     {
  //       path: "",
  //       component: LoginComponent,
  //     },
  //     {
  //       path: "login",
  //       component: LoginComponent,
  //     },
  //     {
  //       path: "register",
  //       component: PasswordOublierComponent,
  //     },
  //     {
  //       path: "logout",
  //       component: ChangerPasswordComponent,
  //     },
  //   ],
  // },
  {
    path: "test",
    component: TestComponent,
  },
  {
    path: "auth",
    component: NbAuthComponent,
    children: [
      {
        path: "",
        component: NbLoginComponent,
      },
      {
        path: "login",
        component: NbLoginComponent,
      },
      {
        path: "register",
        component: NbRegisterComponent,
      },
      {
        path: "logout",
        component: NbLogoutComponent,
      },
      {
        path: "request-password",
        component: NbRequestPasswordComponent,
      },
      {
        path: "reset-password",
        component: NbResetPasswordComponent,
      },
    ],
  },
  {
    path: "reset/:email/:token",
    component: ResetComponent,
  },
  { path: "", redirectTo: "pages", pathMatch: "full" },
  { path: "**", redirectTo: "pages" },
];

const config: ExtraOptions = {
  //useHash: false,
  useHash: true, // pour mettre le # dans l'url apres le nom de domaine
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
