import { UtilitiesService } from "./../services/utilities.service";
import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { environment } from "../../environments/environment";
import { User } from "../models/user";
import { MetiersService } from "../services/metiers.service";
import { Response } from "../models/response";

@Component({
  selector: "ngx-reset",
  templateUrl: "./reset.component.html",
  styleUrls: ["./reset.component.scss"],
})
export class ResetComponent implements OnInit {
  token: any;
  email: any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private utilitiesService: UtilitiesService,
    private metiers: MetiersService
  ) {}
  ngOnInit() {
    // appel de service pour le token

    this.email = this.route.snapshot.params[environment.email];
    this.token = this.route.snapshot.params[environment.token];

    console.log("token", this.token);
    console.log("email", this.email);

    // appel de service

    let data = new User();
    let datas = new Array<User>();
    data.token = this.token;
    data.email = this.email;
    this.metiers
      .authentification(
        //environment.userEnseigne + environment.checkEmailWithTokenIsValid,
        environment.user + environment.checkEmailWithTokenIsValid,
        data
      )
      .subscribe(
        (res) => {
          console.log("res : ", res);
          let response = new Response<any>();
          response = res;
          if (!response.hasError) {
            console.log("res : ", res);
            datas = response.items;
            this.router.navigateByUrl(
              environment.authentification +
                environment.CChangerPassword +
                "/" +
                this.email
            );

            //this.utilitiesService.connexionLocalStorage(datas[0]);
          } else {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusDanger
            );
            // navigation
            this.router.navigateByUrl(
              environment.pages + environment.CTokenInvalide
            );
            console.log("erreur", res["status"]);
          }
        },
        (error) => {
          console.log("Erreur de connexion. Veuillez reessayer !!!");
        }
      );
  }
}
