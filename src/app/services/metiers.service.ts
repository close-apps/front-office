import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { Request } from "../models/request";
import { HttpClient } from "@angular/common/http";
import { environment } from "../../environments/environment";
import { UtilitiesService } from "./utilities.service";

@Injectable({
  providedIn: "root",
})
export class MetiersService {
  constructor(
    private http: HttpClient,
    private utilitiesService: UtilitiesService
  ) {}

  getByCriteria(
    table = "",
    data = {},
    datas = null,
    isAnd = false
  ): Observable<any> {
    let request = new Request<any>();
    //request.user =  localStorage.getItem("user");
    request.user = this.utilitiesService.userId;
    request.data = data;
    if (datas != null) {
      request.datas = datas;
    }
    if (isAnd != null) {
      request.isAnd = isAnd;
    }

    console.log("request envoyé: ", request);
    return this.http.post(
      environment.BASE_URL + table + environment.getByCriteria,
      request,
      environment.httpOptions
    );
  }

  getByCriteriaForStats(
    table = "",
    data = {},
    datas = null,
    isAnd = false
  ): Observable<any> {
    let request = new Request<any>();
    //request.user =  localStorage.getItem("user");
    request.user = this.utilitiesService.userId;
    request.data = data;
    if (datas != null) {
      request.datas = datas;
    }
    if (isAnd != null) {
      request.isAnd = isAnd;
    }
    request.stat = true;

    console.log("request envoyé: ", request);
    return this.http.post(
      environment.BASE_URL + table + environment.getByCriteria,
      request,
      environment.httpOptions
    );
  }

  getBasicStat(
    table = "",
    data = {},
    datas = null,
    isAnd = false
  ): Observable<any> {
    let request = new Request<any>();
    //request.user =  localStorage.getItem("user");
    request.user = this.utilitiesService.userId;
    request.data = data;
    if (datas != null) {
      request.datas = datas;
    }
    if (isAnd != null) {
      request.isAnd = isAnd;
    }
    console.log("request envoyé: ", request);
    return this.http.post(
      environment.BASE_URL + table + environment.getBasicStat,
      request,
      environment.httpOptions
    );
  }

  getStatistiques(
    table = "",
    data = {},
    datas = null,
    isAnd = false
  ): Observable<any> {
    let request = new Request<any>();
    //request.user =  localStorage.getItem("user");
    request.user = this.utilitiesService.userId;
    request.data = data;
    if (datas != null) {
      request.datas = datas;
    }
    if (isAnd != null) {
      request.isAnd = isAnd;
    }
    console.log("request envoyé: ", request);
    return this.http.post(
      environment.BASE_URL + table + environment.getStatistiques,
      request,
      environment.httpOptions
    );
  }

  getByCriteriaCustom(
    table = "",
    data = {},
    datas = null,
    isAnd = false
  ): Observable<any> {
    let request = new Request<any>();
    //request.user =  localStorage.getItem("user");
    request.user = this.utilitiesService.userId;
    request.data = data;
    if (datas != null) {
      request.datas = datas;
    }
    if (isAnd != null) {
      request.isAnd = isAnd;
    }
    console.log("request envoyé: ", request);
    return this.http.post(
      environment.BASE_URL + table + environment.getByCriteriaCustom,
      request,
      environment.httpOptions
    );
  }

  authentification(
    endpoint = "",
    data = {},
    datas = null,
    isAnd = false
  ): Observable<any> {
    let request = new Request<any>();
    //request.user =  localStorage.getItem("user");
    //request.user = this.utilitiesService.userId;
    request.data = data;
    if (datas != null) {
      request.datas = datas;
    }
    if (isAnd != null) {
      request.isAnd = isAnd;
    }
    console.log("request envoyé: ", request);
    return this.http.post(
      environment.BASE_URL + endpoint,
      request,
      environment.httpOptions
    );
  }

  edit(endpoint = "", datas = []): Observable<any> {
    let request = new Request<any>();
    //request.user =  localStorage.getItem("user");
    request.user = this.utilitiesService.userId;
    request.datas = datas;
    return this.http.post(
      environment.BASE_URL + endpoint,
      request,
      environment.httpOptions
    );
  }

  create(table = "", datas = []): Observable<any> {
    let request = new Request<any>();
    //request.user =  localStorage.getItem("user");
    request.user = this.utilitiesService.userId;
    request.datas = datas;
    return this.http.post(
      environment.BASE_URL + table + environment.create,
      request,
      environment.httpOptions
    );
  }
  update(table = "", datas = []): Observable<any> {
    let request = new Request<any>();
    //request.user =  localStorage.getItem("user");
    request.user = this.utilitiesService.userId;
    request.datas = datas;
    return this.http.post(
      environment.BASE_URL + table + environment.update,
      request,
      environment.httpOptions
    );
  }
  bloquer(table = "", datas = []): Observable<any> {
    let request = new Request<any>();
    //request.user =  localStorage.getItem("user");
    request.user = this.utilitiesService.userId;
    request.datas = datas;
    return this.http.post(
      environment.BASE_URL + table + environment.update,
      request,
      environment.httpOptions
    );
  }
  delete(table = "", datas = []): Observable<any> {
    let request = new Request<any>();
    //request.user =  localStorage.getItem("user");
    request.user = this.utilitiesService.userId;
    request.datas = datas;
    return this.http.post(
      environment.BASE_URL + table + environment.delete,
      request,
      environment.httpOptions
    );
  }

  forceDelete(table = "", datas = []): Observable<any> {
    let request = new Request<any>();
    request.user = this.utilitiesService.userId;
    request.datas = datas;
    return this.http.post(
      environment.BASE_URL + table + environment.forceDelete,
      request,
      environment.httpOptions
    );
  }

  checkEmailWithTokenIsValid(data = {}): Observable<any> {
    let request = new Request<any>();
    //request.user = this.user ;
    //request.user = localStorage.getItem("user");
    //this.request.datas = datas ;
    request.data = data;
    console.log("request envoyé: ", request);
    //console.log("request envoyé: ", this.request);
    return this.http.post(
      environment.BASE_URL +
        environment.user +
        environment.checkEmailWithTokenIsValid,
      request,
      environment.httpOptions
    );
  }
}
