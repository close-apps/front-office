import { TestBed } from '@angular/core/testing';

import { IsConnectedService } from './is-connected.service';

describe('IsConnectedService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IsConnectedService = TestBed.get(IsConnectedService);
    expect(service).toBeTruthy();
  });
});
