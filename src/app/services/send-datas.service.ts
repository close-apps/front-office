import { Injectable } from "@angular/core";
import { User } from "../models/user";

@Injectable({
  providedIn: "root",
})
export class SendDatasService {
  constructor() {}

  data: any;
  isPDFTampon;

  clear() {
    this.data = null;
    this.isPDFTampon = null;
  }
}
