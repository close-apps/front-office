import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
} from "@angular/router";
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: "root",
})
export class IsConnectedService {
  constructor(private router: Router) {}
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log("::::: canActivate :::::");
    if (localStorage.getItem(environment.isAuth)) {
      // deja connecter une fois
      this.router.navigateByUrl(environment.pageTableauDeBord);
      //"/home/" + localStorage.getItem(environment.user) + "/home"
      //"/home/" + localStorage.getItem(environment.user)
    }
    return true;
  }
}
