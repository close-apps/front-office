import { async } from "@angular/core/testing";
import { Router } from "@angular/router";
import { Injectable } from "@angular/core";
import { environment } from "../../environments/environment";
import {
  NbGlobalPosition,
  NbGlobalPhysicalPosition,
  NbComponentStatus,
  NbToastrService,
  NbDialogService,
} from "@nebular/theme";
import { NgForm } from "@angular/forms";
import { DialogNamePromptComponent } from "../pages/modal-overlays/dialog/dialog-name-prompt/dialog-name-prompt.component";
import { Location } from "@angular/common";
import { User } from "../models/user";

@Injectable({
  providedIn: "root",
})
export class UtilitiesService {
  constructor(
    private toastrService: NbToastrService,
    private router: Router,
    private location: Location,
    private dialogService: NbDialogService
  ) {}

  // debut notification *****************
  position: NbGlobalPosition = NbGlobalPhysicalPosition.TOP_RIGHT;
  preventDuplicates = false;
  status: NbComponentStatus = "primary";
  destroyByClick = true;
  hasIcon = true;
  icon = "src/favicon.png";
  statusPrimary: NbComponentStatus = "primary";
  statusSuccess: NbComponentStatus = "success";
  statusWarning: NbComponentStatus = "warning";
  statusDanger: NbComponentStatus = "danger";
  statusInfo: NbComponentStatus = "info";
  names = [];

  // fin notification *****************

    connexionLocalStorage(userConnected) {
    if (userConnected != null) {
      localStorage.setItem(environment.user, userConnected.id);
      localStorage.setItem(environment.isAuth, "true");
      localStorage.setItem(environment.enseigneId, userConnected.enseigneId);
      //localStorage.setItem(environment.enseigneId, userConnected.enseigneId);
      localStorage.setItem(environment.login, userConnected.login);
      localStorage.setItem(environment.nom, userConnected.nom);

      // todo : à ne pas oublier, les champs qui peuvent etre vide les verifier avant de les setter dasn le localstorage
      if (userConnected.isAdminEnseigne) {
        localStorage.setItem(
          environment.isAdminEnseigne,
          userConnected.isAdminEnseigne
        );
      }
      if (userConnected.firstPointDeVenteId) {
        localStorage.setItem(
          environment.pointDeVenteId,
          userConnected.firstPointDeVenteId
        );
      }
      if (userConnected.urlLogo) {
        localStorage.setItem(environment.urlLogo, userConnected.urlLogo);
      }
      if (userConnected.color) {
        localStorage.setItem(environment.color, userConnected.color);
      }
    }
  }

  deconnexionLocalStorage() {
    localStorage.removeItem(environment.user);
    localStorage.removeItem(environment.isAuth);
    localStorage.removeItem(environment.login);
    localStorage.removeItem(environment.enseigneId);
    localStorage.removeItem(environment.pointDeVenteId);
    localStorage.removeItem(environment.nom);
    localStorage.removeItem(environment.isAdminEnseigne);
    localStorage.removeItem(environment.color);
    localStorage.removeItem(environment.urlLogo);
  }

  showToast(
    body: string,
    status: NbComponentStatus = this.statusPrimary,
    title: string = "retour"
  ) {
    //private showToast(type: NbComponentStatus, title: string, body: string) {
    const config = {
      //status: this.status,
      //status: this.status,
      //icon: this.icon,
      status: status,
      destroyByClick: this.destroyByClick,
      duration: environment.TEMPS_TOAST,
      hasIcon: this.hasIcon,
      position: this.position,
      preventDuplicates: this.preventDuplicates,
    };
    const titleContent = title ? `. ${title}` : "";

    //this.index += 1;
    this.toastrService.show(body, `Message ${titleContent}`, config);
  }

  resetForm(form: NgForm) {
    if (form != null) {
      form.resetForm();
    }
  }

  begin(content: string) {
    console.log(
      "********************** BEGIN : ",
      content,
      "**********************"
    );
  }

  end(content: string) {
    console.log(
      "********************** END : ",
      content,
      "**********************"
    );
  }

  deleteElementInList(
    list: Array<any>,
    elementASupprimer: any,
    nbreElement = 1
  ): Array<any> {
    list.splice(list.indexOf(elementASupprimer), nbreElement); // pour supprimer l'element
    return [...list];
  }

  openDialog() {
    this.dialogService
      .open(DialogNamePromptComponent)
      .onClose.subscribe((name) => name && this.names.push(name));
  }

  reloadPage() {
    this.router
      // skipLocationChange : When true, navigates without pushing a new state into history.
      // (Pas de retour possible en cliquant sur le bouton précédent du navigateur)
      .navigateByUrl("/", { skipLocationChange: true }) // 1. rediriger vers l'url racine
      .then(() => {
        console.log(decodeURI(this.location.path())); // 2. rediregier vers l'url courant
        // rediriger vers la page courante
        this.router.navigate([decodeURI(this.location.path())]);
      });

    // finalement on à juste simulé un le fait de charger une autre route et revenir sur la courante pour rechager le composant :)
  }

  open3() {
    this.dialogService
      .open(DialogNamePromptComponent)
      .onClose.subscribe((name) => name && this.names.push(name));
  }
  get enseigneId() {
    return localStorage.getItem(environment.enseigneId);
  }
  get pointDeVenteId() {
    return localStorage.getItem(environment.pointDeVenteId);
  }
  get userId() {
    return localStorage.getItem(environment.user);
  }
  get loginUser() {
    return localStorage.getItem(environment.login);
  }
  get nomUser() {
    return localStorage.getItem(environment.nom);
  }
  get isAdminEnseigne() {
    return localStorage.getItem(environment.isAdminEnseigne);
  }

  get valueIsAdminEnseigne() {
    let retour = false;
    if (this.isAdminEnseigne && this.isAdminEnseigne == "true") {
      retour = true;
    }
    return retour;
  }
  isNotEmpty(datas: []) {
    return datas && datas.length > 0;
  }

  async toBase64(file) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      //reader.readAsArrayBuffer(file);
      reader.onload = () => resolve(reader.result); // declenché si la lecture est terminée avec succès

      /*
      reader.onload = () => {
        let encoded = reader.result.toString().replace(/^data:(.*,)?/
        , "");
        if (encoded.length % 4 > 0) {
          encoded += "=".repeat(4 - (encoded.length % 4));
        }
        resolve(encoded);
      };
      */
      reader.onerror = (error) => reject(error); // declenché si on rencontre une erreur lors de la lecture
    });
  }

  async getToBase64(file) {
    //await this.toBase64(file);
    let result = await this.toBase64(file).catch((e) => Error(e));
    if (result instanceof Error) {
      console.log("Error: ", result.message);
      return "";
    }
    console.log("result : ", result);
    /*
    console.log(
      "result  in base64: ",
      result.toString().replace(/^data:(.*,)?/, "")
    );
    */
    return result.toString().replace(/^data:(.*,)?/, "");
  }

  async isSelectFile(event, datasFichier, inputChamp) {
    console.log("debut isSelectFile");

    let file = event.target.files[0];
    let fichierBase64 = await this.getToBase64(file);
    if (!fichierBase64 || fichierBase64.length == 0) {
      this.showToast("Erreur de chargement. Veuillez réessayer !!! ");
      return datasFichier;
    }

    let dataFichier = new User();
    let fullNameFile = file.name;
    let nameAndExtension = fullNameFile.split(".");
    let extension = nameAndExtension[nameAndExtension.length - 1];
    let name = fullNameFile.split("." + extension)[0];

    dataFichier.name = name;
    dataFichier.urlLogo = name;
    dataFichier.fichierBase64 = fichierBase64;
    dataFichier.extension = extension;
    dataFichier.extensionLogo = extension;

    // nombre de fichier et ajout dans la liste

    if (!datasFichier) {
      datasFichier = [];
    }

    const index: number = datasFichier.findIndex(
      (file) => file["name"] == name
    );

    if (index !== -1) {
      this.showToast("Impossible de charger le meme fichier");
      return datasFichier;
    }

    datasFichier.push(dataFichier);

    //console.log("fin isSelectFile");

    //$("#" + inputChamp).val(""); //vider le input
    return datasFichier;
  }

  dateToString(date: any): string {
    //let date = new Date(this.dateDepot) ;
    console.log("date envoyé : ", date);
    return (
      date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear()
    );
  }
}
