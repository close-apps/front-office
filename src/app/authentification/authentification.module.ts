import { ECommerceModule } from "./../pages/e-commerce/e-commerce.module";
import { DashboardModule } from "./../pages/dashboard/dashboard.module";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { AuthentificationRoutingModule } from "./authentification-routing.module";
import { LoginComponent } from "./login/login.component";
import { InscriptionComponent } from "./inscription/inscription.component";
import { DemandeInscriptionComponent } from "./demande-inscription/demande-inscription.component";
import { PasswordOublierComponent } from "./password-oublier/password-oublier.component";
import { ChangerPasswordComponent } from "./changer-password/changer-password.component";
import { AuthentificationComponent } from "./authentification.component";
import { MiscellaneousModule } from "../pages/miscellaneous/miscellaneous.module";
import { ThemeModule } from "../@theme/theme.module";
import {
  NbMenuModule,
  NbLayoutModule,
  NbAlertModule,
  NbIconModule,
  NbCheckboxModule,
  NbCardModule,
  NbInputModule,
  NbButtonModule,
  NbActionsModule,
  NbUserModule,
  NbRadioModule,
  NbDatepickerModule,
  NbSelectModule,
} from "@nebular/theme";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NbEvaIconsModule } from "@nebular/eva-icons";
import { LogoutUserComponent } from "./logout-user/logout-user.component";

@NgModule({
  declarations: [
    LoginComponent,
    InscriptionComponent,
    DemandeInscriptionComponent,
    PasswordOublierComponent,
    ChangerPasswordComponent,
    AuthentificationComponent,
    LogoutUserComponent,
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    CommonModule,
    AuthentificationRoutingModule,
    ThemeModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    NbCardModule,
    NbInputModule,
    NbEvaIconsModule,

    NbAlertModule,
    NbIconModule,
    NbCheckboxModule,
    NbLayoutModule,
    NbButtonModule,
    NbActionsModule,
    NbUserModule,
    NbRadioModule,
    NbDatepickerModule,
    NbSelectModule,
    NbMenuModule,
  ],
})
export class AuthentificationModule {}
