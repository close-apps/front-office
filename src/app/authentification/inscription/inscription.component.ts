import { Component, OnInit } from "@angular/core";
import { MetiersService } from "../../services/metiers.service";
import { Router } from "@angular/router";
import { UtilitiesService } from "../../services/utilities.service";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { NgForm } from "@angular/forms";
import { User } from "../../models/user";

@Component({
  selector: "ngx-inscription",
  templateUrl: "./inscription.component.html",
  styleUrls: ["./inscription.component.scss"],
})
export class InscriptionComponent implements OnInit {
  constructor(
    private router: Router,
    private metiersService: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}
  showPassword = true;
  isSubmitted = false;

  model = new User();

  datasRole = [
    { id: 1, libelle: "Role 1" },
    { id: 2, libelle: "Role 2" },
    { id: 3, libelle: "Role 3" },
    { id: 4, libelle: "Role 4" },
    { id: 5, libelle: "Role 5" },
  ];

  datasEnseigne = [
    { id: 1, nom: "Enseigne 1" },
    { id: 2, nom: "Enseigne 2" },
    { id: 3, nom: "Enseigne 3" },
    { id: 4, nom: "Enseigne 4" },
    { id: 5, nom: "Enseigne 5" },
  ];

  ngOnInit() {
    // récupération de la liste des roles
    this.metiersService.getByCriteria(environment.role).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;

        if (response != null && !response.hasError && response.items != null) {
          this.datasRole = response.items;
        }
      },
      (err) => {
        console.log(err);
      }
    );

    // récupération de la liste des enseignes
    this.metiersService.getByCriteria(environment.enseigne).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;

        if (response != null && !response.hasError && response.items != null) {
          this.datasEnseigne = response.items;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  submit(monModelForm: NgForm) {
    let response = new Response<any>();

    // desactiver le button du formulaire
    this.isSubmitted = monModelForm.submitted;
    console.log(monModelForm.value);

    // appel service
    this.metiersService
      //.create(environment.userEnseigne, [monModelForm.value])
      .create(environment.user, [monModelForm.value])
      .subscribe(
        (res) => {
          response = res;
          console.log(response);
          if (response != null && !response.hasError) {
            // maj du localStorage
            this.utilitiesService.connexionLocalStorage(
              response.items != null ? response.items[0] : null
            );

            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            // navigation etant connecter
            this.router.navigateByUrl(environment.pageAccueil);
            //this.router.navigateByUrl("../pages/dashboard");

            // reset form
            this.utilitiesService.resetForm(monModelForm);
          } else {
            if (response != null) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusDanger
              );
            }
            // activer le button du formulaire
            this.isSubmitted = false;
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
          this.isSubmitted = false;
        }
      );
  }
}
