import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-logout-user",
  templateUrl: "./logout-user.component.html",
  styleUrls: ["./logout-user.component.scss"],
})
export class LogoutUserComponent implements OnInit {
  constructor(
    private utilitiesService: UtilitiesService,
    private router: Router
  ) {}

  ngOnInit() {
    console.log("deconnexion");
    this.utilitiesService.deconnexionLocalStorage();
    this.router.navigateByUrl(environment.pageAccueil);
  }
}
