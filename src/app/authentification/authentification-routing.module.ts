import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthentificationComponent } from "./authentification.component";
import { LoginComponent } from "./login/login.component";
import { InscriptionComponent } from "./inscription/inscription.component";
import { DemandeInscriptionComponent } from "./demande-inscription/demande-inscription.component";
import { ChangerPasswordComponent } from "./changer-password/changer-password.component";
import { PasswordOublierComponent } from "./password-oublier/password-oublier.component";
import { LogoutUserComponent } from "./logout-user/logout-user.component";

const routes: Routes = [
  {
    path: "",
    component: AuthentificationComponent,
    children: [
      {
        path: "logout-user",
        component: LogoutUserComponent,
      },
      {
        path: "login",
        component: LoginComponent,
      },
      {
        path: "inscription",
        component: InscriptionComponent,
      },
      {
        path: "password-oublier",
        component: PasswordOublierComponent,
      },
      {
        path: "changer-password/:email",
        component: ChangerPasswordComponent,
      },
      {
        path: "changer-password",
        component: ChangerPasswordComponent,
      },
      {
        path: "demande-inscription",
        component: DemandeInscriptionComponent,
      },
      {
        path: "",
        redirectTo: "login",
        pathMatch: "full",
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthentificationRoutingModule {}
