import { Component, OnInit } from "@angular/core";
import { MetiersService } from "../../services/metiers.service";
import { Router } from "@angular/router";
import { UtilitiesService } from "../../services/utilities.service";
import { NgForm } from "@angular/forms";
import { Response } from "../../models/response";
import { environment } from "../../../environments/environment";

@Component({
  selector: "ngx-password-oublier",
  templateUrl: "./password-oublier.component.html",
  styleUrls: ["./password-oublier.component.scss"],
})
export class PasswordOublierComponent implements OnInit {
  constructor(
    private router: Router,
    private metiersService: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}
  showPassword = true;
  isSubmitted = false;

  ngOnInit() {}

  submit(monModelForm: NgForm) {
    let response = new Response<any>();

    // desactiver le button du formulaire
    this.isSubmitted = monModelForm.submitted;
    console.log(monModelForm.value);

    // appel service
    this.metiersService
      .authentification(
        //environment.userEnseigne + environment.passwordOublier,
        environment.user + environment.passwordOublier,
        monModelForm.value
      )
      .subscribe(
        (res) => {
          response = res;
          console.log(response);
          if (response != null && !response.hasError) {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            // navigation
            this.router.navigateByUrl(environment.pageAccueil);
            //this.router.navigateByUrl("../pages/dashboard");

            // reset form
            this.utilitiesService.resetForm(monModelForm);
          } else {
            if (response != null) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusDanger
              );
            }
            // activer le button du formulaire
            this.isSubmitted = false;
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
          this.isSubmitted = false;
        }
      );
  }
}
