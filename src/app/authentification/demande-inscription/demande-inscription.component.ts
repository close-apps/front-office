import { async } from "@angular/core/testing";
import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";
import { Response } from "../../models/response";
import { NgForm } from "@angular/forms";
import { environment } from "../../../environments/environment";
import { User } from "../../models/user";

@Component({
  selector: "ngx-demande-inscription",
  templateUrl: "./demande-inscription.component.html",
  styleUrls: ["./demande-inscription.component.scss"],
})
export class DemandeInscriptionComponent implements OnInit {
  constructor(
    private router: Router,
    private metiersService: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}
  showPassword = true;
  isSubmitted = false;
  datasSecteurActiviteSelected: any;
  datasRole;
  datasSecteurActivite;
  // datasRole = [
  //   { id: 1, libelle: "Role 1" },
  //   { id: 2, libelle: "Role 2" },
  //   { id: 3, libelle: "Role 3" },
  //   { id: 4, libelle: "Role 4" },
  //   { id: 5, libelle: "Role 5" },
  // ];
  // datasSecteurActivite = [
  //   { id: 1, libelle: "Secteur 1" },
  //   { id: 2, libelle: "Secteur 2" },
  //   { id: 3, libelle: "Secteur 3" },
  //   { id: 4, libelle: "Secteur 4" },
  //   { id: 5, libelle: "Secteur 5" },
  // ];
  model = new User();

  ngOnInit() {
    // récupération de la liste des roles

    let data = new User();
    data.code = environment.ADMINISTRATEUR;
    data.codeParam = { operator: "<>" };

    let dataRoleEnseigne = new User();
    dataRoleEnseigne.code = environment.UTILISATEUR_ENSEIGNE;
    dataRoleEnseigne.codeParam = { operator: "<>" };
    let dataRoleMobile = new User();
    dataRoleMobile.code = environment.UTILISATEUR_MOBILE;
    dataRoleMobile.codeParam = { operator: "<>" };

    let datas = [];
    datas.push(dataRoleEnseigne);
    datas.push(dataRoleMobile);

    this.metiersService
      .getByCriteria(environment.role, data, datas, true)
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;

          if (
            response != null &&
            !response.hasError &&
            response.items != null
          ) {
            this.datasRole = response.items;
          }
        },
        (err) => {
          console.log(err);
        }
      );

    // récupération de la liste des secteurs d'activités
    this.metiersService.getByCriteria(environment.secteurActivite).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;

        if (response != null && !response.hasError && response.items != null) {
          this.datasSecteurActivite = response.items;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  async submit(monModelForm: NgForm) {
    let response = new Response<any>();

    // desactiver le button du formulaire
    this.isSubmitted = monModelForm.submitted;
    console.log("monModelForm.value", monModelForm.value);
    console.log(
      "datasSecteurActiviteSelected",
      this.datasSecteurActiviteSelected
    );

    this.model = monModelForm.value;
    console.log("model  avant", this.model);

    this.model.datasSecteurActivite = this.datasSecteurActiviteSelected;

    //console.log("model  apres", this.model);

    //console.log("monModelForm.value 2", monModelForm.value);

    // appel service
    this.metiersService.create(environment.enseigne, [this.model]).subscribe(
      (res) => {
        response = res;
        console.log(response);
        if (response != null && !response.hasError) {
          // notification
          this.utilitiesService.showToast(
            response.status.message,
            this.utilitiesService.statusSuccess
          );
          // navigation
          this.router.navigateByUrl(environment.pageAccueil);
          //this.router.navigateByUrl("../pages/dashboard");

          // reset form
          this.utilitiesService.resetForm(monModelForm);
        } else {
          if (response != null) {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusDanger
            );
          }
          // activer le button du formulaire
          this.isSubmitted = false;
        }
      },
      (err) => {
        console.log(err);
        this.utilitiesService.showToast(
          environment.erreurDeConnexion,
          this.utilitiesService.statusInfo
        );
        this.isSubmitted = false;
      }
    );
  }
}
