import { UtilitiesService } from "./../../services/utilities.service";
import { MetiersService } from "./../../services/metiers.service";
import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { NgForm } from "@angular/forms";
import { User } from "../../models/user";

@Component({
  selector: "ngx-changer-password",
  templateUrl: "./changer-password.component.html",
  styleUrls: ["./changer-password.component.scss"],
})
export class ChangerPasswordComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private metiersService: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}
  showPassword = true;
  isSubmitted = false;
  isResetByEmail = false;
  confirmPassword = "";
  email = "";

  model = new User();

  ngOnInit() {
    this.email = this.route.snapshot.params[environment.email];
    if (this.email) {
      this.isResetByEmail = true;
    }
  }

  // controle pr verifier que la confirmation du newPasswxord

  submit(monModelForm: NgForm) {
    let response = new Response<any>();

    // desactiver le button du formulaire
    this.isSubmitted = monModelForm.submitted;
    console.log(monModelForm.value);

    //this.model = monModelForm.value;

    if (this.isResetByEmail) {
      this.model.email = this.email;

      // appel services
      this.metiersService
        .authentification(
          environment.user + environment.changerPasswordOublier,
          //environment.userEnseigne + environment.changerPasswordOublier,
          this.model
        )
        .subscribe(
          (res) => {
            response = res;
            console.log(response);
            if (response != null && !response.hasError) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusSuccess
              );

              // setter que le user est connecte
              this.utilitiesService.connexionLocalStorage(response.items[0]);

              // navigation
              this.router.navigateByUrl(environment.pageAccueil);
              //this.router.navigateByUrl("../pages/dashboard");

              // reset form
              this.utilitiesService.resetForm(monModelForm);
            } else {
              if (response != null) {
                // notification
                this.utilitiesService.showToast(
                  response.status.message,
                  this.utilitiesService.statusDanger
                );
              }
              // activer le button du formulaire
              this.isSubmitted = false;
            }
          },
          (err) => {
            console.log(err);
            this.utilitiesService.showToast(
              environment.erreurDeConnexion,
              this.utilitiesService.statusInfo
            );
            this.isSubmitted = false;
          }
        );
    } else {
      //this.model.login = localStorage.getItem(environment.login);
      this.model.login = this.utilitiesService.loginUser;
      if (this.model.login != null && this.model.login.length > 0) {
        // appel service

        this.model.newPassword = this.model.newPassword.trim();
        this.model.password = this.model.password.trim();
        this.metiersService
          .authentification(
            environment.user + environment.passwordOublier,
            //environment.userEnseigne + environment.passwordOublier,
            this.model
          )
          .subscribe(
            (res) => {
              response = res;
              console.log(response);
              if (response != null && !response.hasError) {
                // notification
                this.utilitiesService.showToast(
                  response.status.message,
                  this.utilitiesService.statusSuccess
                );
                // navigation
                this.router.navigateByUrl(environment.pageAccueil);
                //this.router.navigateByUrl("../pages/dashboard");

                let user = new User();
                user = response.items[0];
                // localStorage
                this.utilitiesService.connexionLocalStorage(user);

                // reset form
                this.utilitiesService.resetForm(monModelForm);
              } else {
                if (response != null) {
                  // notification
                  this.utilitiesService.showToast(
                    response.status.message,
                    this.utilitiesService.statusDanger
                  );
                }
                // activer le button du formulaire
                this.isSubmitted = false;
              }
            },
            (err) => {
              console.log(err);
              this.utilitiesService.showToast(
                environment.erreurDeConnexion,
                this.utilitiesService.statusInfo
              );
              this.isSubmitted = false;
            }
          );
      } else {
        // notification
        this.utilitiesService.showToast(
          "Vous devez etre connecté d'abord. Merci de vous connecter !!!",
          this.utilitiesService.statusInfo
        );
        // activer le button du formulaire
        this.isSubmitted = false;
      }
    }
  }
}
