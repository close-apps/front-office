import { User } from "./../../models/user";
import { environment } from "./../../../environments/environment";
import { MetiersService } from "./../../services/metiers.service";
import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { NbAuthSocialLink, NbAuthService } from "@nebular/auth";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { Response } from "../../models/response";
import { UtilitiesService } from "../../services/utilities.service";
import { NbComponentStatus } from "@nebular/theme";

@Component({
  selector: "ngx-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  showPassword = true;
  isSubmitted = false;
  pagePasswordOublier = environment.pagePasswordOublier;
  //pagePasswordOublier = "/pages/dashboard";

  protected service: NbAuthService;
  protected options: {};
  protected cd: ChangeDetectorRef;
  redirectDelay: number;
  showMessages: any;
  strategy: string;
  errors: string[];
  messages: string[];
  user: User;

  submitted: boolean;
  socialLinks: NbAuthSocialLink[];
  rememberMe: boolean;

  constructor(
    private router: Router,
    private metiersService: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}

  ngOnInit() {}

  login() {}
  getConfigValue(key: string) {}

  getInputType() {
    if (this.showPassword) {
      return "text";
    }
    return "password";
  }

  toggleShowPassword() {
    this.showPassword = !this.showPassword;
  }

  submit(monModelForm: NgForm) {
    let response = new Response<any>();
    let data = new User();

    this.isSubmitted = monModelForm.submitted;
    console.log(monModelForm.value);

    data = monModelForm.value;

    data.login = data.login.trim();
    data.password = data.password.trim();

    // appel service
    this.metiersService
      .authentification(
        //environment.userEnseigne + environment.connexion,
        environment.user + environment.connexion,
        //monModelForm.value
        data
      )
      .subscribe(
        (res) => {
          response = res;
          console.log("response : ", response);
          if (response != null && !response.hasError) {
            let userConnected = new User();
            userConnected = response.items[0];
            // maj du localStorage
            this.utilitiesService.connexionLocalStorage(
              response.items != null ? userConnected : null
            );
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            console.log("userConnected : ", userConnected);
            // navigation
            if (userConnected != null && userConnected.isFirstlyConnexion) {
              //this.router.navigateByUrl(environment.pageAccueil);

              this.router.navigateByUrl(
                environment.authentification +
                  environment.CChangerPassword +
                  "/" +
                  userConnected.email
              );
            } else {
              this.router.navigateByUrl(environment.pageAccueil);
            }
            //this.router.navigateByUrl("../pages/dashboard");

            // reset form
            this.utilitiesService.resetForm(monModelForm);
          } else {
            if (response != null) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusDanger
              );
            }
            // desactiver le button
            this.isSubmitted = false;
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
          this.isSubmitted = false;
        }
      );
  }
}
