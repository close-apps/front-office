import { Entreprise } from "./entreprise";

export class User {
  //constructor(){};

  id: any;
  userPDVId;
  villeId: number;
  isScanManuel;
  userId;
  enseigneId: any;
  enseigneIdForTampon: any;
  enseigneIdForCarte: any;
  roleId: number;
  continentId: number;
  paysId: number;
  compteurOffreNew: number;
  compteurOffreUpdate: number;
  totalCompteur: number;
  compteurAppelDoffresNew: number;
  compteurAppelDoffresSelect: number;
  compteurAppelDoffresUpdate: number;
  typeEntrepriseCode: any;
  conditionsDutilisation: any;
  nom: string;
  prenoms: string;
  message: string;
  gain: string;
  description: any;
  chiffreDaffaire;
  communeId;
  userLogin;
  dateDebut;
  dateFin;

  statutJuridiqueId: number;

  //isDispoTamponAllPdv: boolean;
  //isDispoCarteAllPdv: boolean;

  isDispoInAllPdv: any;
  isAllCarteDipso: any;
  isAllTamponDipso: any;

  nbreProgrammeFideliteTampon: number;
  firstPointDeVenteId;
  pointDeVenteId: any;
  nbreProgrammeFideliteCarte: number;
  nbreUserEnseigne: number;
  // isDispoTamponAllPdv = true;
  // isDispoCarteAllPdv = true;

  nbreMaxiParPdv: number;
  nbreMaxiAllPdv: number;
  nbrePDV: number;
  nbreUtilisateur: number;

  carteId;

  uniteTempsId: number;
  uniteTempsLibelle: string;
  datasInformationRegleSecuriteTampon: any;
  datasInformationRegleSecuriteCarte: any;
  datasPointDeVente: any;
  datasProgrammeFideliteCarte: any;
  datasProgrammeFideliteTampon: any;
  dataGpsPointDeVente: any;
  dataUserEnseigne: any;
  datasUserEnseigne: any;

  typeRegleSecuriteTampon: any;
  longititude: any;
  latitude: any;
  uniteTemps: any;

  titre: any;
  ordre: any;
  nbrePallier: number;
  valeurMaxGain: number;
  nbreTampon: number;

  pourXAcheter: number;
  nbrePointObtenu: number;
  correspondanceNbrePoint: number;
  pointObtenuParParrain: number;
  pointObtenuParFilleul: number;
  urlCarte: string;
  typeProgrammeFideliteCarteId: number;
  programmeFideliteCarteId: any;
  programmeFideliteTamponId: any;
  nbreFoisCrediterParPdv: number;
  nbreFoisCrediterAllPdv: number;
  nbreFoisDebiter: number;
  plafondSommeACrediter: number;
  tamponObtenuApresSouscription: number;
  pointObtenuApresSouscription: number;
  typeActionCode;
  somme;
  contenuScanner;
  typeActionLibelle;

  isSelected: any;
  token: any;
  code: string;
  libelle: string;
  prenom: string;
  email: string;
  telephone: string;
  login: string;
  password: string;
  newPassword: string;
  roleCode: string;
  test: any;
  // dataEntreprise = new Entreprise();
  // datasVille = new Array<User>();
  // fonctionnalites: Array<any>;

  dataEntreprise: any;
  datasVille: any;
  fonctionnalites: any;
  contenu: string;
  typeContenuStatiqueLibelle: string;
  typeContenuStatiqueCode: string;
  codeParam: any;

  isFirstlyConnexion: boolean;
  paysLibelle: any;
  paysCode: any;

  datasSecteurActivite: any;

  name: any;
  urlLogo: any;
  nomFichier: any;

  fichierBase64: any;
  extension: any;
  extensionLogo: any;
  facebook;
  roleGerantId;
  instagram;
  twitter;
  urlImage;
}
