import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-editer-pdfcarte-enseigne",
  templateUrl: "./editer-pdfcarte-enseigne.component.html",
  styleUrls: ["./editer-pdfcarte-enseigne.component.scss"],
})
export class EditerPdfcarteEnseigneComponent implements OnInit {
  datasPointDeVenteSelected: any;
  datasPointDeVente: any;
  //datasTypeRegleSecuriteCarte: any;
  //data;
  datasUniteTemps: any;
  datasInformationRegleSecuriteCarte = []; // pr la liste des regles de securite
  notChangeLogo = true;

  // datasInformationRegleSecuriteCarte = [
  //   {
  //     id: 1,
  //     typeRegleSecuriteCarteLibelle: "Règle 2",
  //     typeRegleSecuriteCarteId: 2,
  //     uniteTempsLibelle: "Par jour",
  //     uniteTempsId: 1,
  //     nbreMaxiParPdv: 5,
  //     nbreMaxiAllPdv: 10,
  //   },
  // ]; // pr la liste des regles de securite

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private utilitiesService: UtilitiesService,
    private metiersService: MetiersService
  ) {}

  model = new User();
  disableAjouter = false;
  doublonExiste = false;

  isSubmitted = false;
  programmeFideliteCarteId: any;
  modelInformationRegleSecuriteCarte = new User();

  optionsRadio = [
    { value: "true", label: "Oui", checked: true },
    { value: "false", label: "Non" },
  ];
  actionSubmit = environment.CREER;

  ngOnInit() {
    this.initValueRadioInModel();

    this.programmeFideliteCarteId = this.route.snapshot.params[environment.id];

    if (this.programmeFideliteCarteId) {
      this.actionSubmit = environment.MODIFIER;
      // different de null
      // recuperer l'enseigne en question
      // récupération de la liste des roles
      let data = new User();
      data.id = this.programmeFideliteCarteId;
      this.metiersService
        .getByCriteria(environment.programmeFideliteCarte, data)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;

            if (
              response != null &&
              !response.hasError &&
              response.items != null
            ) {
              this.model = response.items[0];

              // setter les datas pour la maj
              this.datasPointDeVenteSelected = this.model.datasPointDeVente;
              if (
                this.model.datasInformationRegleSecuriteCarte &&
                this.model.datasInformationRegleSecuriteCarte != null
              ) {
                this.datasInformationRegleSecuriteCarte =
                  this.model.datasInformationRegleSecuriteCarte;
              }
              // update radio
              if (this.model.isDispoInAllPdv) {
                this.optionsRadio[0].checked = this.model.isDispoInAllPdv;
                this.optionsRadio[1].checked = !this.model.isDispoInAllPdv;
              }
            }
          },
          (err) => {
            console.log(err);
          }
        );
    } else {
      // this.optionsRadio[0].checked = false;
      // this.optionsRadio[1].checked = true;
    }
    //this.model.isDispoCarteAllPdv = true; // setter le nb-radio a true

    // récupération de la liste des pointDeVente
    let enseigneId = this.utilitiesService.enseigneId;
    if (enseigneId) {
      let data = new User();
      data.enseigneId = enseigneId;
      this.metiersService
        .getByCriteria(environment.pointDeVente, data)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            if (
              response != null &&
              !response.hasError &&
              response.items != null
            ) {
              this.datasPointDeVente = response.items;
            }
          },
          (err) => {
            console.log(err);
          }
        );
    }

    // récupération de la liste des uniteTemps
    this.metiersService.getByCriteria(environment.uniteTemps).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;
        if (response != null && !response.hasError && response.items != null) {
          this.datasUniteTemps = response.items;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  submit(monModelForm: NgForm) {
    this.isSubmitted = true; // disable button submit
    this.utilitiesService.begin("submit");

    // ajout des id, data et datas au model
    this.model.enseigneId = this.utilitiesService.enseigneId;
    this.model.datasInformationRegleSecuriteCarte =
      this.datasInformationRegleSecuriteCarte;
    this.model.datasPointDeVente = this.datasPointDeVenteSelected;

    // appel service
    this.metiersService
      .edit(
        environment.programmeFideliteCarte +
          (this.model.id ? environment.update : environment.create),
        [this.model]
      )
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            // navigation retour a la liste des tampons
            this.router.navigateByUrl(
              environment.pages + environment.CPDFCarteEnseigne
            );

            // reset form
            this.utilitiesService.resetForm(monModelForm);
            this.resetDatas();
            // message retour
          } else {
            if (response != null) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusDanger
              );
            }
            // activer le button du formulaire
            this.isSubmitted = false;
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
          this.isSubmitted = false;
        }
      );

    //this.isSubmitted = false; // enable button submit
    this.utilitiesService.end("submit");
  }
  modifierInformationRegleSecurite(data) {
    this.utilitiesService.begin("modifierInformationRegleSecurite");
    console.log(data);
    this.modelInformationRegleSecuriteCarte = data;
    this.supprimerInformationRegleSecurite(data);
  }
  supprimerInformationRegleSecurite(data) {
    // retirer le data dans la liste datasInformationRegleSecuriteCarte
    this.utilitiesService.begin("supprimerInformationRegleSecurite");
    console.log(data);
    this.datasInformationRegleSecuriteCarte =
      this.utilitiesService.deleteElementInList(
        this.datasInformationRegleSecuriteCarte,
        data
      );
  }
  addInformationRegleSecurite() {
    this.disableAjouter = true;
    this.utilitiesService.begin("addInformationRegleSecurite");

    this.datasUniteTemps.forEach((element) => {
      if (element.id == this.modelInformationRegleSecuriteCarte.uniteTempsId) {
        this.modelInformationRegleSecuriteCarte.uniteTempsLibelle =
          element.libelle;
      }
    });

    // verifier les doublons
    let listDoublons = [];

    if (
      this.datasInformationRegleSecuriteCarte != null &&
      this.datasInformationRegleSecuriteCarte.length > 0
    ) {
      listDoublons = this.datasInformationRegleSecuriteCarte.filter(
        (data) =>
          data.uniteTempsLibelle ==
          this.modelInformationRegleSecuriteCarte.uniteTempsLibelle
      );
    }

    if (listDoublons != null && listDoublons.length > 0) {
      // existence de doublons dans la liste
      this.doublonExiste = true;
    } else {
      console.log(
        "this.datasInformationRegleSecuriteCarte: ",
        this.datasInformationRegleSecuriteCarte
      );
      this.datasInformationRegleSecuriteCarte.unshift(
        JSON.parse(JSON.stringify(this.modelInformationRegleSecuriteCarte))
      ); // ajouter a la liste
      // this.datasInformationRegleSecuriteTampon.push(
      //   JSON.parse(JSON.stringify(this.modelInformationRegleSecuriteTampon))
      // ); // ajouter a la liste

      this.modelInformationRegleSecuriteCarte = new User(); //vider le formulaire
      this.doublonExiste = false;
    }
    this.disableAjouter = false;
  }

  changeData() {
    console.log("*********** ", this.modelInformationRegleSecuriteCarte);
  }

  changeIsDispoInAllPdvValue(event) {
    this.utilitiesService.begin("changeIsDispoInAllPdvValue");
    this.model.isDispoInAllPdv = event;
  }

  initValueRadioInModel() {
    this.utilitiesService.begin("initValueRadioInModel");
    if (!this.model.isDispoInAllPdv) {
      this.model.isDispoInAllPdv = "true";
      console.log("this.model.isDispoInAllPdv : ", this.model.isDispoInAllPdv);
    }
  }
  get etatAjouterInfosRegles() {
    return (
      //this.modelInformationRegleSecuriteCarte.nbreMaxiAllPdv &&
      this.modelInformationRegleSecuriteCarte.plafondSommeACrediter &&
      this.modelInformationRegleSecuriteCarte.uniteTempsId
    );
  }

  resetDatas() {
    this.model = new User();
    this.modelInformationRegleSecuriteCarte = new User();
    this.datasPointDeVenteSelected = [];
    this.datasInformationRegleSecuriteCarte = [];
  }
  onChange(event) {
    console.log("onChange event: ", event);
    //this.autres = event;
    console.log(
      "onChange conditionsDutilisation: ",
      this.model.conditionsDutilisation
    );
  }

  async isSelectFileEntreprise(event) {
    console.log("avant this.model : ", this.model);

    let file = event.target.files[0];
    let fichierBase64 = await this.utilitiesService.getToBase64(file);
    if (!fichierBase64 || fichierBase64.length == 0) {
      this.utilitiesService.showToast(
        "Erreur de chargement. Veuillez réessayer !!! "
      );
      return;
    }
    // let fullNameFile = file.name;
    // let nameAndExtension = fullNameFile.split(".");
    // let extension = nameAndExtension[nameAndExtension.length - 1];
    // let name = fullNameFile.split("." + extension)[0];
    //this.dataEntreprise.name = name;
    //this.model.urlLogo = name;
    //this.dataEntreprise.extension = extension;
    //this.model.extensionLogo = extension;

    this.model.nomFichier = file.name;
    this.model.fichierBase64 = fichierBase64;
    console.log("apres this.model : ", this.model);
  }
  changerLogo() {
    console.log("*****changerLogo******");
    this.notChangeLogo = false;
  }
}
