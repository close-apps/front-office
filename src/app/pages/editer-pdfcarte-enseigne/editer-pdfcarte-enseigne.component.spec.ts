import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditerPdfcarteEnseigneComponent } from './editer-pdfcarte-enseigne.component';

describe('EditerPdfcarteEnseigneComponent', () => {
  let component: EditerPdfcarteEnseigneComponent;
  let fixture: ComponentFixture<EditerPdfcarteEnseigneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditerPdfcarteEnseigneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditerPdfcarteEnseigneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
