import { Component, EventEmitter, Input, Output } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";

@Component({
  selector: "ngx-dialog-name-prompt",
  templateUrl: "dialog-name-prompt.component.html",
  styleUrls: ["dialog-name-prompt.component.scss"],
})
export class DialogNamePromptComponent {
  constructor(protected ref: NbDialogRef<DialogNamePromptComponent>) {}

  cancel() {
    console.log("cancel");
    //this.cancelEventEmitter.emit("cancel");
    this.ref.close();
  }

  submit(rep) {
    console.log("submit");
    //this.cancelEventEmitter.emit("submit");
    this.ref.close(rep);
  }
}
