import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { SendDatasService } from "../../services/send-datas.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-scanner-manuellement",
  templateUrl: "./scanner-manuellement.component.html",
  styleUrls: ["./scanner-manuellement.component.scss"],
})
export class ScannerManuellementComponent implements OnInit {
  constructor(
    public sendDatasService: SendDatasService,
    private utilitiesService: UtilitiesService,
    private metiersService: MetiersService,
    private router: Router
  ) {}

  carte;
  itemsPDV;
  itemsPDVProgrammeFideliteCarte;
  itemsPDVProgrammeFideliteTampon;


  // datasTypeAction = [
  //   { code: "DEBITER", libelle: "DEBITER" },
  //   { code: "CREDITER", libelle: "CREDITER" },
  // ];
  datasTypeAction = environment.datasDebitCredit;

  dataTypeAction;
  sommeCarte;

  descriptionCarte;
  descriptionTampon;

  souscriptionTampon;
  souscriptionCarte;

  ngOnInit() {
    this.getDatas();
  }

  changeIsDispoInAllPdvValue(event) {
    this.utilitiesService.begin("changeIsDispoInAllPdvValue");
  }

  getDatas() {
    let carteId = this.sendDatasService.data.id;
    let dataCarte = new User();
    dataCarte.id = carteId;
    this.metiersService
      .getByCriteria(environment.carte, dataCarte)
      //.getByCriteria(environment.user, data)
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          console.log("response : ", response);
          if (response != null && !response.hasError) {
            if (response.items && response.items.length > 0) {
              this.carte = response.items[0];
            }
            //this.dataGpsPointDeVente = this.model.dataGpsPointDeVente;

            // pour griser les PROGRAMMES NON DISPO DANS LE PDV

            if (this.carte) {
              let dataPDV = new User();
              dataPDV.id = this.utilitiesService.pointDeVenteId;
              this.metiersService
                .getByCriteria(environment.pointDeVente, dataPDV)
                .subscribe(
                  (res) => {
                    let responsePDV = new Response<any>();
                    responsePDV = res;
                    console.log("responsePDV : ", responsePDV);
                    if (responsePDV != null && !responsePDV.hasError) {
                      this.itemsPDV = responsePDV.items;
                      if (this.itemsPDV && this.itemsPDV.length > 0) {
                        this.itemsPDV.forEach((itemPDV) => {
                          if (
                            this.carte
                              .datasSouscriptionProgrammeFideliteTampon &&
                            this.carte.datasSouscriptionProgrammeFideliteTampon
                              .length > 0 &&
                            itemPDV.datasProgrammeFideliteTampon &&
                            itemPDV.datasProgrammeFideliteTampon.length > 0
                          ) {
                            itemPDV.datasProgrammeFideliteTampon.forEach(
                              (dataProgrammeFideliteTampon) => {
                                this.carte.datasSouscriptionProgrammeFideliteTampon.forEach(
                                  (dataSouscriptionProgrammeTampon) => {
                                    if (
                                      dataSouscriptionProgrammeTampon.programmeFideliteTamponId ==
                                      dataProgrammeFideliteTampon.id
                                    ) {
                                      dataSouscriptionProgrammeTampon[
                                        "isDisabled"
                                      ] = true;
                                    }
                                  }
                                );
                              }
                            );
                          }

                          if (
                            this.carte
                              .datasSouscriptionProgrammeFideliteCarte &&
                            this.carte.datasSouscriptionProgrammeFideliteCarte
                              .length > 0 &&
                            itemPDV.datasProgrammeFideliteCarte &&
                            itemPDV.datasProgrammeFideliteCarte.length > 0
                          ) {
                            itemPDV.datasProgrammeFideliteCarte.forEach(
                              (dataProgrammeFideliteCarte) => {
                                this.carte.datasSouscriptionProgrammeFideliteCarte.forEach(
                                  (dataSouscriptionProgrammeCarte) => {
                                    if (
                                      dataSouscriptionProgrammeCarte.programmeFideliteTamponId ==
                                      dataProgrammeFideliteCarte.id
                                    ) {
                                      dataSouscriptionProgrammeCarte[
                                        "isDisabled"
                                      ] = true;
                                    }
                                  }
                                );
                              }
                            );
                          }
                        });
                      } else {
                        // TODO: le cas ou il n'y a pas de PDV
                      }

                      //this.dataGpsPointDeVente = this.model.dataGpsPointDeVente;
                    }
                  },
                  (err) => {
                    console.log(err);
                    // this.utilitiesService.showToast(
                    //   environment.erreurDeConnexion,
                    //   this.utilitiesService.statusInfo
                    // );
                  }
                );
            }
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );
  }

  get getNameUser() {
    let name = this.sendDatasService.data.userLogin;
    if (this.sendDatasService.data.userNom) {
      name =
        this.sendDatasService.data.userNom +
        " " +
        this.sendDatasService.data.userPrenoms;
    }

    return name;
  }

  changeSouscriptionPDFCarteValue(event) {
    this.utilitiesService.begin("changeSouscriptionPDFCarteValue");
    console.log("event :::", event);
    this.souscriptionCarte = event;
  }
  changeSouscriptionPDFTamponValue(event) {
    this.utilitiesService.begin("changeSouscriptionPDFTamponValue");
    console.log("event :::", event);
    this.souscriptionTampon = event;
  }
  scanManuelTampon() {
    this.utilitiesService.begin("scanManuelTampon");

    //this.router.navigateByUrl(environment.pageAccueil);

    let data = new User();

    data.id = this.sendDatasService.data.id;
    data.programmeFideliteTamponId =
      this.souscriptionTampon.programmeFideliteTamponId;
    data.pointDeVenteId = this.utilitiesService.pointDeVenteId;

    data.description = this.descriptionTampon;

    // appel service
    this.metiersService
      .edit(environment.carte + environment.flasherTampon, [data])
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            // navigation retour a la liste des tampons
            this.router.navigateByUrl(
              environment.pages + environment.CMesClients
            );

            // reset form
            this.resetDatasTampon();
            // message retour
          } else {
            if (response != null) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusDanger
              );
            }
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );
  }

  scanManuelCarte(resultScan) {
    this.utilitiesService.begin("scanManuelCarte");
    let infosQrCode = resultScan.split(" ");
    let data = new User();

    data.id = this.sendDatasService.data.id;
    data.programmeFideliteCarteId =
      this.souscriptionCarte.programmeFideliteCarteId;
    data.pointDeVenteId = this.utilitiesService.pointDeVenteId;
    data.typeActionCode = this.dataTypeAction.code;
    data.somme = this.sommeCarte;
    //data.contenuScanner = result;
    data.description = this.descriptionCarte;

    // appel service
    this.metiersService
      .edit(environment.carte + environment.flasherCarte, [data])
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            // navigation retour a la liste des tampons
            this.router.navigateByUrl(
              environment.pages + environment.CMesClients
            );

            // reset form
            this.resetDatasCarte();
            // message retour
          } else {
            if (response != null) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusDanger
              );
            }
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );
  }

  resetDatasTampon() {
    this.sommeCarte = null;
    this.dataTypeAction = null;
  }
  resetDatasCarte() {
    this.sommeCarte = null;
    this.dataTypeAction = null;
  }
}
