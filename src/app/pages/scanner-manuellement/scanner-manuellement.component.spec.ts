import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannerManuellementComponent } from './scanner-manuellement.component';

describe('ScannerManuellementComponent', () => {
  let component: ScannerManuellementComponent;
  let fixture: ComponentFixture<ScannerManuellementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScannerManuellementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScannerManuellementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
