import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { SendDatasService } from "../../services/send-datas.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-pdfcarte-enseigne",
  templateUrl: "./pdfcarte-enseigne.component.html",
  styleUrls: ["./pdfcarte-enseigne.component.scss"],
})
export class PdfcarteEnseigneComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private utilitiesService: UtilitiesService,
    private sendDatasService: SendDatasService,
    private metiersService: MetiersService
  ) {}

  // ************* begin declaration des variables  *************
  model = new User();
  //datasPointDeVente: any;
  datasPDFCarte: any;
  programmeFideliteCarteId: any;
  enseigneId: any;
  optionAffichage = environment.ACCUEIL;
  allPDVPourCePDFCarte = environment.allPDVPourCePDFCarte;
  isAdminEnseigne: boolean;
  //datasInformationRegleSecuriteCarte: any; // pr la liste des regles de securite
  datasUserEnDelit;
  // ************* end declaration des variables    *************

  ngOnInit() {
    this.isAdminEnseigne = this.utilitiesService.valueIsAdminEnseigne;

    this.utilitiesService.begin("ngOnInit");
    this.programmeFideliteCarteId = this.route.snapshot.params[environment.id];
    this.enseigneId = this.utilitiesService.enseigneId;
    if (this.programmeFideliteCarteId && this.enseigneId) {
      this.optionAffichage = environment.VOIR;

      // different de null
      // recuperer l'enseigne en question
      // récupération de la liste des roles
      let data = new User();
      data.id = this.programmeFideliteCarteId;
      data.enseigneId = this.enseigneId;
      // TODO:  se rassurer que le user est un admin
      this.metiersService
        .getByCriteria(environment.programmeFideliteCarte, data)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            console.log("response : ", response);
            if (response != null && !response.hasError) {
              this.model = response.items[0];
            }
          },
          (err) => {
            console.log(err);
          }
        );

      this.getUsersEnDelit(data.id);
    } else {
      if (this.enseigneId) {
        // recuperer l'enseigne en question
        // récupération de la liste des roles
        let data = new User();

        if (this.isAdminEnseigne) {
          //data.id = this.programmeFideliteCarteId;
          data.enseigneId = this.enseigneId;
          // TODO:  se rassurer que le user est un admin
          this.metiersService
            .getByCriteria(environment.programmeFideliteCarte, data)
            .subscribe(
              (res) => {
                let response = new Response<any>();
                response = res;
                if (response != null && !response.hasError) {
                  this.datasPDFCarte = response.items;
                }
              },
              (err) => {
                console.log(err);
              }
            );
        } else {
          //data.id = this.programmeFideliteCarteId;
          // data.enseigneId = this.enseigneId;
          data.userPDVId = this.utilitiesService.userId;
          // TODO:  se rassurer que le user est un admin
          this.metiersService
            .getByCriteriaCustom(
              environment.pointDeVenteProgrammeFideliteCarte,
              data
            )
            .subscribe(
              (res) => {
                let response = new Response<any>();
                response = res;
                if (response != null && !response.hasError) {
                  this.datasPDFCarte = response.items;
                }
              },
              (err) => {
                console.log(err);
              }
            );
        }
      } else {
        // merci de vous connectez
        this.utilitiesService.showToast(
          environment.merciDeVousConnectez,
          this.utilitiesService.statusInfo
        );
        // redirection
        this.router.navigateByUrl(environment.pageAccueil);
      }
    }
  }

  showPDFCarte(data) {
    this.router.navigateByUrl(
      environment.pages + environment.CPDFCarteEnseigne + "/" + data.id
    );
  }
  updatePDFCarte(data) {
    this.router.navigateByUrl(
      environment.pages + environment.CEditerPDFCarteEnseigne + "/" + data.id
    );
  }
  createPDFCarte(data) {
    this.router.navigateByUrl(
      environment.pages + environment.CEditerPDFCarteEnseigne
    );
  }
  deletePDFCarte(data) {
    this.utilitiesService.begin("deletePDFCarte");

    // popup

    // delete
    this.metiersService
      .delete(environment.programmeFideliteCarte, [data])
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            this.ngOnInit();
            this.router.navigateByUrl(
              environment.pages + environment.CPDFCarteEnseigne
            );
          } else {
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusDanger
            );
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );
  }

  scannerManuellemnt(data) {
    this.sendDatasService.data = data;
    this.sendDatasService.isPDFTampon = false;
    this.router.navigateByUrl(
      environment.pages + environment.scannerManuellemntParProgramme
    );
  }

  getUsersEnDelit(programmeFideliteCarteId) {
    this.utilitiesService.begin("getUserEnDelit");
    let data = new User();

    data.programmeFideliteCarteId = programmeFideliteCarteId;
    if (!this.utilitiesService.valueIsAdminEnseigne) {
      data.pointDeVenteId = this.utilitiesService.pointDeVenteId;
    }

    this.metiersService
      .getByCriteriaCustom(
        environment.delitProgrammeFidelite,
        data
        //[dataForCarte]
      )
      //.getByCriteria(environment.user, data)
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          console.log("response datasUserEnDelit : ", response);
          if (response != null && !response.hasError) {
            this.datasUserEnDelit = response.items;

            console.log("this.datasUserEnDelit  ::: ", this.datasUserEnDelit);

            //this.dataGpsPointDeVente = this.model.dataGpsPointDeVente;
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );
  }

  showUserEnDelit(dataUser) {
    this.utilitiesService.begin("showUserEnDelit");
    this.sendDatasService.data = dataUser;
    this.sendDatasService.data.pdfId = this.model.id;
    this.sendDatasService.isPDFTampon = false;
    this.router.navigateByUrl(environment.pages + environment.CDetailsClients);
  }
}
