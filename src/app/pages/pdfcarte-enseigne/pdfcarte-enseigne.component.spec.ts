import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdfcarteEnseigneComponent } from './pdfcarte-enseigne.component';

describe('PdfcarteEnseigneComponent', () => {
  let component: PdfcarteEnseigneComponent;
  let fixture: ComponentFixture<PdfcarteEnseigneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdfcarteEnseigneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdfcarteEnseigneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
