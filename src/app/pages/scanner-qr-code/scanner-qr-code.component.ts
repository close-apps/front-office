import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { QrScannerComponent } from "angular2-qrscanner";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-scanner-qr-code",
  templateUrl: "./scanner-qr-code.component.html",
  styleUrls: ["./scanner-qr-code.component.scss"],
})
export class ScannerQrCodeComponent implements OnInit {
  constructor(
    private router: Router,
    private metiersService: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}

  @ViewChild(QrScannerComponent, { static: false })
  qrScannerComponent: QrScannerComponent;


  // datasTypeAction = [
  //   { code: "DEBITER", libelle: "DEBITER" },
  //   { code: "CREDITER", libelle: "CREDITER" },
  // ];
  datasTypeAction = environment.datasDebitCredit;

  //datasTypeAction;
  somme;
  description;
  dataTypeAction;
  isVisibleScan = false;
  isDebiter = true;
  ngOnInit() {
    // this.metiersService.getByCriteria(environment.typeAction).subscribe(
    //   (res) => {
    //     let response = new Response<any>();
    //     response = res;
    //     if (response != null && !response.hasError && response.items != null) {
    //       this.datasTypeAction = response.items;
    //     }
    //   },
    //   (err) => {
    //     console.log(err);
    //   }
    // );
  }

  changeIsVisibleScan() {}

  ngAfterViewInit() {
    this.qrScannerComponent.getMediaDevices().then((devices) => {
      console.log("devices : ", devices);
      const videoDevices: MediaDeviceInfo[] = [];
      for (const device of devices) {
        if (device.kind.toString() === "videoinput") {
          videoDevices.push(device);
        }
      }
      if (videoDevices.length > 0) {
        let choosenDev;
        for (const dev of videoDevices) {
          if (dev.label.includes("front")) {
            choosenDev = dev;
            break;
          }
        }
        if (choosenDev) {
          this.qrScannerComponent.chooseCamera.next(choosenDev);
        } else {
          this.qrScannerComponent.chooseCamera.next(videoDevices[0]);
        }
      }
    });

    this.qrScannerComponent.capturedQr.subscribe((result) => {
      console.log("result : ", result);
      //this.router.navigateByUrl(environment.pageAccueil);

      let infosQrCode = result.split(" ");
      let data = new User();

      data.id = infosQrCode[0];
      data.programmeFideliteCarteId = infosQrCode[1];
      data.pointDeVenteId = this.utilitiesService.pointDeVenteId;
      data.typeActionCode = this.dataTypeAction.code;
      data.somme =
        infosQrCode.length > 2 && infosQrCode[2] != null
          ? infosQrCode[2]
          : this.somme;
      //data.contenuScanner = result;
      data.description = this.description;

      // appel service
      this.metiersService
        .edit(environment.carte + environment.flasherCarte, [data])
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            if (response != null && !response.hasError) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusSuccess
              );
              // navigation retour a la liste des tampons
              this.router.navigateByUrl(
                environment.pages + environment.CMesClients
              );

              // reset form
              this.resetDatas();
              // message retour
            } else {
              if (response != null) {
                // notification
                this.utilitiesService.showToast(
                  response.status.message,
                  this.utilitiesService.statusDanger
                );
              }
            }
          },
          (err) => {
            console.log(err);
            this.utilitiesService.showToast(
              environment.erreurDeConnexion,
              this.utilitiesService.statusInfo
            );
          }
        );
    });
  }

  changeAction(event) {
    console.log("changeAction event: ", event);
    console.log("changeAction event.code: ", event.code);
    if (event && event.code != environment.CODEDEBITER) {
      this.isDebiter = false;
    } else {
      this.isDebiter = true;
    }
  }

  onKeySomme(event) {
    console.log("onKeySomme event: ", event);
    this.isVisibleScan = true;
  }
  resetDatas() {
    this.somme = null;
    this.dataTypeAction = null;
  }
}
