import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZxingScannerQrTamponComponent } from './zxing-scanner-qr-tampon.component';

describe('ZxingScannerQrTamponComponent', () => {
  let component: ZxingScannerQrTamponComponent;
  let fixture: ComponentFixture<ZxingScannerQrTamponComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZxingScannerQrTamponComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZxingScannerQrTamponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
