import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ZXingScannerComponent } from "@zxing/ngx-scanner";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-zxing-scanner-qr-tampon",
  templateUrl: "./zxing-scanner-qr-tampon.component.html",
  styleUrls: ["./zxing-scanner-qr-tampon.component.scss"],
})
export class ZxingScannerQrTamponComponent implements OnInit {
  description;
  libelleActionScan = environment.afficherScannerQrCode;
  isActiverScan = false;
  isScannerOk = false;

  @ViewChild("scanner", { static: false })
  scanner: ZXingScannerComponent;

  desiredDevice;
  scannerEnabled = true;
  torch;
  messageAttenteApresScan = environment.messageAttenteApresScan;

  constructor(
    private router: Router,
    private metiersService: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}

  ngOnInit() {}

  camerasNotFoundHandler(event) {
    this.utilitiesService.begin("camerasNotFoundHandler");
    console.log("event :::: ", event);
  }
  scanSuccessHandler(event) {
    this.utilitiesService.begin("scanSuccessHandler");
    console.log("event :::: ", event);
    this.scannerEnabled = false;
    this.isScannerOk = true;

    //this.scanner.scanStop();
    this.sendScan(event);
  }
  camerasFoundHandler(event) {
    this.utilitiesService.begin("camerasFoundHandler");
    console.log("event :::: ", event);
  }
  onTorchCompatible(event) {
    this.utilitiesService.begin("onTorchCompatible");
    console.log("event :::: ", event);
  }
  scanCompleteHandler(event) {
    this.utilitiesService.begin("scanCompleteHandler");
    console.log("event :::: ", event);
  }
  scanFailureHandler(event) {
    this.utilitiesService.begin("scanFailureHandler");
    console.log("event :::: ", event);
  }
  scanErrorHandler(event) {
    this.utilitiesService.begin("scanErrorHandler");
    console.log("event :::: ", event);
  }

  activerScan(value) {
    this.isActiverScan = value;

    if (value) {
      this.libelleActionScan = environment.cacherScannerQrCode;
    } else {
      this.libelleActionScan = environment.afficherScannerQrCode;
    }
  }
  sendScan(resultScann) {
    console.log("resultScann ::: ", resultScann);
    //this.router.navigateByUrl(environment.pageAccueil);

    let infosQrCode = resultScann.split(" ");
    let data = new User();

    console.log("infosQrCode ::: ", infosQrCode);

    if (infosQrCode[0]) {
      data.id = infosQrCode[0];
    }
    if (infosQrCode[1]) {
      data.programmeFideliteTamponId = infosQrCode[1];
    }
    data.pointDeVenteId = this.utilitiesService.pointDeVenteId;

    data.description = this.description;

    // appel service
    this.metiersService
      .edit(environment.carte + environment.flasherTampon, [data])
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            // navigation retour a la liste des tampons
            this.router.navigateByUrl(
              environment.pages + environment.CMesClients
            );

            // reset form
            //this.resetDatas();
            // message retour
          } else {
            if (response != null && response.hasError) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusDanger
              );
              this.initScanner();
            }
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexionOuDataIncorrect,
            this.utilitiesService.statusDanger
          );
          this.initScanner();
        }
      );
  }
  initScanner() {
    this.utilitiesService.begin("initScanner");
    this.isScannerOk = false;
    this.scannerEnabled = true;
    //this.scanner.reset();
    //this.scanner.restart();
  }
}
