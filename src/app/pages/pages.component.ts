import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "../../environments/environment";
import { UtilitiesService } from "../services/utilities.service";

import { MENU_ITEMS } from "./pages-menu";

@Component({
  selector: "ngx-pages",
  templateUrl: "./pages.component.html",
  styleUrls: ["pages.component.scss"],
})
export class PagesComponent {
  basicMenu = [
    {
      //libelle: environment.ACCUEIL,
      libelle: environment.Application,
      link: environment.pageAccueil,
      childrens: [],
    },
    {
      libelle: environment.Pro,
      link: environment.pages + environment.CPro,
      childrens: [],
    },
    {
      libelle: environment.confidentialite,
      link: environment.pages + environment.CConfidentialite,
      childrens: [],
    },
    {
      libelle: environment.nousContacter,
      link: environment.pages + environment.CNousContacter,
      childrens: [],
    },
    // {
    //   libelle: environment.autres,
    //   //link: environment.pages + environment.CEncours,
    //   childrens: [
    //     { libelle: "Autre 1", link: environment.pages + environment.CEncours },
    //     { libelle: "Autre 2", link: environment.pages + environment.CEncours },
    //     { libelle: "Autre 3", link: environment.pages + environment.CEncours },
    //   ],
    // },
  ];

  userMenu = [
    {
      libelle: environment.tableauDeBord,
      link: environment.pageTableauDeBord,
      childrens: [],
    },
    {
      libelle: environment.mesPDFCarte,
      //link: environment.pageAccueil,
      link: environment.pages + environment.CPDFCarteEnseigne,
      childrens: [],
    },
    {
      libelle: environment.mesPDFTampon,
      //link: environment.pageAccueil,
      link: environment.pages + environment.CPDFTamponEnseigne,
      childrens: [],
    },
    {
      libelle: environment.mesPDV,
      link: environment.pages + environment.CPDVEnseigne,
      childrens: [],
    },
    {
      libelle: environment.mesClients,
      link: environment.pages + environment.CMesClients,
      childrens: [],
    },
    {
      libelle: environment.scannerCarte,
      link: environment.pages + environment.CScannerQrCode,
      childrens: [],
    },
    {
      libelle: environment.scannerTampon,
      link: environment.pages + environment.CScannerQrCodeTampon,
      childrens: [],
    },
    {
      libelle: environment.monProfil,
      link: environment.pages + environment.CMajProfilEnseigne,
      childrens: [],
    },
  ];

  basicProfil = [
    {
      libelle: environment.seConnecter,
      link: environment.authentification + environment.CLogin,
      childrens: [],
    },
    {
      libelle: environment.nousRejoindre,
      link: environment.authentification + environment.CDemandeInscription,
      childrens: [],
    },
  ];

  userProfil = [
    {
      libelle: "",
      //link: environment.pages + environment.CMajProfil,
      childrens: [
        {
          libelle: environment.profilUser,
          link: environment.pages + environment.CMajProfilUser,
          //click: "",
          childrens: [],
        },
        {
          libelle: environment.changerPassword,
          link: environment.pages + environment.CChangerPassword,
          //click: "",
          childrens: [],
        },
        {
          libelle: environment.deconnexion,
          link: environment.authentification + environment.CLogoutUser,
          //click: "",
          childrens: [],
        },
      ],
    },
  ];
  menu = MENU_ITEMS;
  isAuth = false;
  login: string;
  constructor(
    private utilitiesService: UtilitiesService,
    private router: Router
  ) {}

  menuPrincipal: any;
  menuSecondaire: any;
  urlLogo: any;
  color: any;
  ngOnInit() {
    console.log(
      "localStorage.getItem(environment.isAuth) : ",
      localStorage.getItem(environment.isAuth)
    );
    this.init();
  }

  deconnexion() {
    console.log("deconnexion");
    this.utilitiesService.deconnexionLocalStorage();
    this.router.navigateByUrl(environment.pageAccueil);
  }
  profil(login: string) {}

  init() {
    console.log("init");
    this.isAuth = Boolean(localStorage.getItem(environment.isAuth));
    if (this.isAuth) {
      //this.login = localStorage.getItem(environment.login);
      this.login = this.utilitiesService.loginUser;

      // gerer le menuPrincipal et secondaire
      let indexMenuMonProfil;
      for (let i = 0; i < this.userMenu.length; i++) {
        if (this.userMenu[i].libelle == environment.monProfil) {
          indexMenuMonProfil = i;
          break;
        }
      }

      // setter le id de l enseigne connecter pour la maj du profil
      if (indexMenuMonProfil && indexMenuMonProfil > 0) {
        // rendre visible ou pas l'option Profil enseigne selon le type du user connecte
        if (this.utilitiesService.valueIsAdminEnseigne) {
          this.userMenu[indexMenuMonProfil].link +=
            "/" + this.utilitiesService.enseigneId;
        } else {
          this.userMenu.splice(indexMenuMonProfil);
        }
      }
      this.menuPrincipal = this.userMenu;
      //this.userProfil[0].libelle = localStorage.getItem(environment.login);
      //this.userProfil[0].libelle = this.utilitiesService.loginUser; // setter le login du user connecter
      this.userProfil[0].libelle = this.utilitiesService.nomUser; // setter le nom du user connecter

      this.userProfil[0].childrens[0].link +=
        "/" + this.utilitiesService.userId; // setter le id du user connecter pour la maj du profil
      this.menuSecondaire = this.userProfil;
      //this.menuPrincipal = this.userProfil;

      this.urlLogo = localStorage.getItem(environment.urlLogo)
        ? localStorage.getItem(environment.urlLogo)
        : environment.defaultUrlLogo;
      this.color = localStorage.getItem(environment.color)
        ? localStorage.getItem(environment.color)
        : environment.defaultColor;
    } else {
      this.menuPrincipal = this.basicMenu;
      this.menuSecondaire = this.basicProfil;
      this.urlLogo = environment.defaultUrlLogo;
      this.color = environment.defaultColor;
    }
  }
}
