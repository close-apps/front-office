import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MajProfilUserComponent } from './maj-profil-user.component';

describe('MajProfilUserComponent', () => {
  let component: MajProfilUserComponent;
  let fixture: ComponentFixture<MajProfilUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MajProfilUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajProfilUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
