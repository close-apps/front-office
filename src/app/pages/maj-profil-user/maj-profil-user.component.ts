import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-maj-profil-user",
  templateUrl: "./maj-profil-user.component.html",
  styleUrls: ["./maj-profil-user.component.scss"],
})
export class MajProfilUserComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private metiersService: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}
  showPassword = true;
  isSubmitted = false;
  userEnseigneId;

  model = new User();

  datasRole;

  datasEnseigne;

  ngOnInit() {
    this.userEnseigneId = this.route.snapshot.params[environment.id];
    if (this.userEnseigneId) {
      // recuperer l'enseigne en question
      // récupération de la liste des roles
      this.model.id = this.userEnseigneId;
      this.metiersService
        //.getByCriteria(environment.userEnseigne, this.model)
        .getByCriteria(environment.user, this.model)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;

            if (
              response != null &&
              !response.hasError &&
              response.items != null
            ) {
              this.model = response.items[0];
            }
          },
          (err) => {
            console.log(err);
          }
        );
    } else {
    }
  }

  submit(monModelForm: NgForm) {
    let response = new Response<any>();

    // desactiver le button du formulaire
    this.isSubmitted = monModelForm.submitted;
    console.log(monModelForm.value);

    // appel service
    this.metiersService
      .update(environment.user, [this.model])
      //.update(environment.userEnseigne, [this.model])
      .subscribe(
        (res) => {
          response = res;
          console.log(response);
          if (response != null && !response.hasError) {
            // maj du localStorage
            this.utilitiesService.connexionLocalStorage(
              response.items != null ? response.items[0] : null
            );

            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            // navigation etant connecter
            this.router.navigateByUrl(environment.pageAccueil);
            //this.router.navigateByUrl("../pages/dashboard");

            // reset form
            this.utilitiesService.resetForm(monModelForm);
          } else {
            if (response != null) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusDanger
              );
            }
            // activer le button du formulaire
            this.isSubmitted = false;
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
          this.isSubmitted = false;
        }
      );
  }
}
