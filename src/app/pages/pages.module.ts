import { CKEditorModule } from "ng2-ckeditor";
import { NgModule } from "@angular/core";
import {
  NbMenuModule,
  NbLayoutModule,
  NbCardModule,
  NbButtonModule,
  NbInputModule,
  NbSelectModule,
  NbAlertModule,
  NbIconModule,
  NbCheckboxModule,
  NbActionsModule,
  NbUserModule,
  NbRadioModule,
  NbDatepickerModule,
  NbListModule,
  NbTabsetModule,
  NbAccordionModule,
  NbStepperModule,
} from "@nebular/theme";

import { ThemeModule } from "../@theme/theme.module";
import { PagesComponent } from "./pages.component";
import { DashboardModule } from "./dashboard/dashboard.module";
import { ECommerceModule } from "./e-commerce/e-commerce.module";
import { PagesRoutingModule } from "./pages-routing.module";
import { MiscellaneousModule } from "./miscellaneous/miscellaneous.module";
import { TokenInvalideComponent } from "./token-invalide/token-invalide.component";
import { MajProfilComponent } from "./maj-profil-enseigne/maj-profil.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { NbEvaIconsModule } from "@nebular/eva-icons";
import { MajProfilUserComponent } from "./maj-profil-user/maj-profil-user.component";
import { PdvEnseigneComponent } from "./pdv-enseigne/pdv-enseigne.component";
import { PdftamponEnseigneComponent } from "./pdftampon-enseigne/pdftampon-enseigne.component";
import { PdfcarteEnseigneComponent } from "./pdfcarte-enseigne/pdfcarte-enseigne.component";
import { SouscripitionEnseigneComponent } from "./souscripition-enseigne/souscripition-enseigne.component";
import { EditerPdftamponEnseigneComponent } from "./editer-pdftampon-enseigne/editer-pdftampon-enseigne.component";
import { EditerPdfcarteEnseigneComponent } from "./editer-pdfcarte-enseigne/editer-pdfcarte-enseigne.component";
import { EditerPdvEnseigneComponent } from "./editer-pdv-enseigne/editer-pdv-enseigne.component";
import { NgxEchartsModule } from "ngx-echarts";
import { MapsModule } from "./maps/maps.module";
import { ScannerQrCodeComponent } from "./scanner-qr-code/scanner-qr-code.component";
import { NgQrScannerModule } from "angular2-qrscanner";
import { MesClientsComponent } from "./mes-clients/mes-clients.component";
import { Ng2SmartTableModule } from "ng2-smart-table";
import { DetailsClientComponent } from "./details-client/details-client.component";
import { ScannerQrCodeTamponComponent } from "./scanner-qr-code-tampon/scanner-qr-code-tampon.component";
import { EditorsModule } from "./editors/editors.module";
import { AccueilComponent } from "./accueil/accueil.component";
import { NousContacterComponent } from "./nous-contacter/nous-contacter.component";
import { ConfidentialiteComponent } from "./confidentialite/confidentialite.component";
import { LayoutModule } from "./layout/layout.module";
import { ProComponent } from "./pro/pro.component";
import { CarouselModule } from "ngx-owl-carousel-o";
import { ZxingScannerQrCarteComponent } from "./zxing-scanner-qr-carte/zxing-scanner-qr-carte.component";
import { ZXingScannerModule } from "@zxing/ngx-scanner";
import { ZxingScannerQrTamponComponent } from "./zxing-scanner-qr-tampon/zxing-scanner-qr-tampon.component";
import { ScannerManuellementComponent } from "./scanner-manuellement/scanner-manuellement.component";
import { ScannerManuellementParProgrammeComponent } from "./scanner-manuellement-par-programme/scanner-manuellement-par-programme.component";
import { ModalOverlaysModule } from "./modal-overlays/modal-overlays.module";
import { DialogNamePromptCustomComponent } from "./dialog-name-prompt-custom/dialog-name-prompt-custom.component";
import { DialogNamePromptDebloquerComponent } from "./dialog-name-prompt-debloquer/dialog-name-prompt-debloquer.component";
import { DetailsHistoriqueClientComponent } from "./details-historique-client/details-historique-client.component";
import { TableauDeBordComponent } from "./tableau-de-bord/tableau-de-bord.component";
import { ChartsModule } from "./charts/charts.module";
import { ChartModule } from "angular2-chartjs";
import { NgxChartsModule } from "@swimlane/ngx-charts";
//import { TestScanComponent } from "./test-scan/test-scan.component";
//import { ZXingScannerModule } from "@zxing/ngx-scanner";

@NgModule({
  imports: [
    ReactiveFormsModule,
    FormsModule,
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    NbLayoutModule,
    NbCardModule,
    NbButtonModule,
    NbInputModule,
    NbSelectModule,
    NbEvaIconsModule,
    NbAlertModule,
    NbIconModule,
    NbCheckboxModule,
    NbActionsModule,
    NbUserModule,
    NbRadioModule,
    NbDatepickerModule,
    NbTabsetModule,
    Ng2SmartTableModule,
    NbListModule,
    NgxEchartsModule, // pour les graphes
    ChartModule, // pour les graphes
    MapsModule,
    NgQrScannerModule,
    EditorsModule, // a cause des importations dans le ts du CkEditorComponent
    NbAccordionModule,
    NbStepperModule,

    CKEditorModule,
    //ZXingScannerModule, // pour le scan
    CarouselModule,
    ZXingScannerModule,
    ModalOverlaysModule,
  ],
  declarations: [
    PagesComponent,
    TokenInvalideComponent,
    MajProfilComponent,
    MajProfilUserComponent,
    PdvEnseigneComponent,
    PdftamponEnseigneComponent,
    PdfcarteEnseigneComponent,
    SouscripitionEnseigneComponent,
    EditerPdftamponEnseigneComponent,
    EditerPdfcarteEnseigneComponent,
    EditerPdvEnseigneComponent,
    ScannerQrCodeComponent,
    MesClientsComponent,
    DetailsClientComponent,
    ScannerQrCodeTamponComponent,
    AccueilComponent,
    NousContacterComponent,
    ConfidentialiteComponent,
    ProComponent,
    ZxingScannerQrCarteComponent,
    ZxingScannerQrTamponComponent,
    ScannerManuellementComponent,
    ScannerManuellementParProgrammeComponent,
    DialogNamePromptCustomComponent,
    DialogNamePromptDebloquerComponent,
    DetailsHistoriqueClientComponent,
    TableauDeBordComponent,
    // TestScanComponent,
  ],
  entryComponents: [
    DialogNamePromptCustomComponent,
    DialogNamePromptDebloquerComponent,
  ], // important pour les popup
})
export class PagesModule {}
