import { Component, OnInit } from "@angular/core";
import { User } from "../../models/user";

@Component({
  selector: "ngx-nous-contacter",
  templateUrl: "./nous-contacter.component.html",
  styleUrls: ["./nous-contacter.component.scss"],
})
export class NousContacterComponent implements OnInit {
  constructor() {}
  model = new User();
  isSubmitted = false;

  ngOnInit() {}

  submit(monModelForm) {
    this.isSubmitted = true;
    // appel de service

    this.isSubmitted = false;
  }
}
