import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-pro",
  templateUrl: "./pro.component.html",
  styleUrls: ["./pro.component.scss"],
})
export class ProComponent implements OnInit {
  constructor(
    private router: Router,
    private utilitiesService: UtilitiesService,
    private metiersService: MetiersService
  ) {}

  datasSouscription: any;

  ngOnInit() {
    // récupération de la liste des souscriptions

    let data = new User();
    this.metiersService.getByCriteria(environment.souscription, data).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;
        if (response != null && !response.hasError && response.items != null) {
          this.datasSouscription = response.items;
          console.log("response.items ProComponent : ", response.items);
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getPrix(prix) {
    return isNaN(prix) ? prix : prix + "/mois";
  }

  nousRejoindre() {
    this.router.navigateByUrl(
      environment.authentification + environment.CDemandeInscription
    );
  }
}
