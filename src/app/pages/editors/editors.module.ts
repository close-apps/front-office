import { NgModule } from "@angular/core";
import { NbCardModule } from "@nebular/theme";
import { CKEditorModule } from "ng2-ckeditor";

import { ThemeModule } from "../../@theme/theme.module";

import {
  EditorsRoutingModule,
  routedComponents,
} from "./editors-routing.module";

@NgModule({
  imports: [NbCardModule, ThemeModule, EditorsRoutingModule, CKEditorModule],
  declarations: [...routedComponents],
  exports: [...routedComponents], // Tres tres important dans un module. surtout si on veut reeutiliser ces cimposnat dans les autres modules
})
export class EditorsModule {}
