import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { ParametragesService } from "../../services/parametrages.service";
import { SendDatasService } from "../../services/send-datas.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-mes-clients",
  templateUrl: "./mes-clients.component.html",
  styleUrls: ["./mes-clients.component.scss"],
})
export class MesClientsComponent implements OnInit {
  constructor(
    private parametrages: ParametragesService,
    private metiersService: MetiersService,
    private router: Router,
    private utilities: UtilitiesService,
    private sendDatasService: SendDatasService
  ) {}

  datasUser: any;
  ngOnInit() {
    // recuperation des listes deroulantes
    this.initDatas();
  }

  initDatas() {
    // let dataForTampon = new User();
    // let dataForCarte = new User();
    //
    //dataForTampon.enseigneIdForTampon = this.utilities.enseigneId;
    // dataForTampon.pointDeVenteId = this.utilities.pointDeVenteId;
    //dataForCarte.enseigneIdForCarte = this.utilities.enseigneId;
    // dataForCarte.pointDeVenteId = this.utilities.pointDeVenteId;

    let data = new User();

    data.enseigneId = this.utilities.enseigneId;
    // if (!this.utilities.valueIsAdminEnseigne) {
    //   data.pointDeVenteId = this.utilities.pointDeVenteId;
    // }

    this.metiersService
      .getByCriteriaCustom(
        environment.souscriptionProgrammeFidelite,
        data
        //[dataForCarte]
      )
      //.getByCriteria(environment.user, data)
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          console.log("response : ", response);
          if (response != null && !response.hasError) {
            this.datasUser = response.items;
            //this.dataGpsPointDeVente = this.model.dataGpsPointDeVente;
          }
        },
        (err) => {
          console.log(err);
          this.utilities.showToast(
            environment.erreurDeConnexion,
            this.utilities.statusInfo
          );
        }
      );
  }

  scannerManuellemnt(data) {
    this.sendDatasService.data = data;
    this.router.navigateByUrl(
      environment.pages + environment.scannerManuellemnt
    );
  }
  voirUser(data) {
    this.sendDatasService.data = data;
    this.router.navigateByUrl(
      environment.pages + environment.CDetailsHistoriqueClient
    );
  }
  getUserEnDelit() {
    this.utilities.begin("getUserEnDelit");
    let data = new User();

    if (!this.utilities.valueIsAdminEnseigne) {
      data.pointDeVenteId = this.utilities.pointDeVenteId;
    }

    this.metiersService
      .getByCriteriaCustom(
        environment.delitProgrammeFidelite,
        data
        //[dataForCarte]
      )
      //.getByCriteria(environment.user, data)
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          console.log("response : ", response);
          if (response != null && !response.hasError) {
            this.datasUser = response.items;
            //this.dataGpsPointDeVente = this.model.dataGpsPointDeVente;
          }
        },
        (err) => {
          console.log(err);
          this.utilities.showToast(
            environment.erreurDeConnexion,
            this.utilities.statusInfo
          );
        }
      );
  }
}
