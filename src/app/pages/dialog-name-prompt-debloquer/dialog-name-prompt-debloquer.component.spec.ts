import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogNamePromptDebloquerComponent } from './dialog-name-prompt-debloquer.component';

describe('DialogNamePromptDebloquerComponent', () => {
  let component: DialogNamePromptDebloquerComponent;
  let fixture: ComponentFixture<DialogNamePromptDebloquerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogNamePromptDebloquerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogNamePromptDebloquerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
