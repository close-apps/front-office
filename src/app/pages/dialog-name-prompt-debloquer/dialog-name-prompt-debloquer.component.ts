import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: "ngx-dialog-name-prompt-debloquer",
  templateUrl: "./dialog-name-prompt-debloquer.component.html",
  styleUrls: ["./dialog-name-prompt-debloquer.component.scss"],
})
export class DialogNamePromptDebloquerComponent implements OnInit {
  constructor(protected ref: NbDialogRef<DialogNamePromptDebloquerComponent>) {}

  titre;
  DialogNamePromptComponent(titre) {}
  cancel(rep) {
    console.log("cancel");
    //this.cancelEventEmitter.emit("cancel");
    this.ref.close(rep);
  }

  submit(rep) {
    console.log("submit");
    //this.cancelEventEmitter.emit("submit");
    this.ref.close(rep);
  }

  ngOnInit() {}
}
