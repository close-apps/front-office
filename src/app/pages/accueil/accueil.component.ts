import { Component, OnInit } from "@angular/core";
import { OwlOptions } from "ngx-owl-carousel-o";

@Component({
  selector: "ngx-accueil",
  templateUrl: "./accueil.component.html",
  styleUrls: ["./accueil.component.scss"],
})
export class AccueilComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  customOptions: OwlOptions = {
    loop: true,
    margin: 20,
    //stagePadding: 10,
    nav: true,
    smartSpeed: 1000,

    items: 5,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    autoplay: true,
    freeDrag: true,
    autoWidth: false,
    //center: true,
    dots: false,
    navSpeed: 700,
    navText: ["", ""],
    responsive: {
      0: {
        items: 1,
      },
      400: {
        items: 2,
      },
      740: {
        items: 3,
      },
      1080: {
        items: 5,
      },
      1920: {
        items: 5,
      },
    },
  };

  slidesStore = [
    {
      id: "0",
      src: "assets/images/screen0.png",
      title: "",
      alt: "",
    },
    {
      id: "1",
      src: "assets/images/screen1.png",
      title: "",
      alt: "",
    },
    {
      id: "11",
      src: "assets/images/screen11.png",
      title: "",
      alt: "",
    },
    {
      id: "2",
      src: "assets/images/screen2.jpg",
      title: "",
      alt: "",
    },
    {
      id: "3",
      src: "assets/images/screen3.jpg",
      title: "",
      alt: "",
    },
    {
      id: "4",
      src: "assets/images/screen4.png",
      title: "",
      alt: "",
    },
    {
      id: "12",
      src: "assets/images/screen0.png",
      title: "",
      alt: "",
    },
    {
      id: "5",
      src: "assets/images/screen1.png",
      title: "",
      alt: "",
    },
    {
      id: "6",
      src: "assets/images/screen2.jpg",
      title: "",
      alt: "",
    },
    {
      id: "7",
      src: "assets/images/screen3.jpg",
      title: "",
      alt: "",
    },
    {
      id: "8",
      src: "assets/images/screen4.png",
      title: "",
      alt: "",
    },
    {
      id: "9",
      src: "assets/images/screen1.png",
      title: "",
      alt: "",
    },
    {
      id: "10",
      src: "assets/images/screen2.jpg",
      title: "",
      alt: "",
    },
  ];
}
