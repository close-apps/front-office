import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { NbComponentStatus } from "@nebular/theme";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";
import { Location } from "../maps/search-map/entity/Location";

@Component({
  selector: "ngx-editer-pdv-enseigne",
  templateUrl: "./editer-pdv-enseigne.component.html",
  styleUrls: ["./editer-pdv-enseigne.component.scss"],
})
export class EditerPdvEnseigneComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private utilitiesService: UtilitiesService,
    private metiersService: MetiersService
  ) {}

  // ************* begin declaration des variables    *************
  optionsRadio = [
    { value: "true", label: "Oui", checked: true },
    { value: "false", label: "Non" },
  ];
  radioAjouterUser = "true";
  radioCarte = "true"; // pour gerer les checked par defaut
  radioTampon = "true"; // pour gerer les checked par defaut
  datasProgrammeFideliteCarte: any;
  datasProgrammeFideliteCarteSelected: any;
  datasProgrammeFideliteTampon: any;
  datasProgrammeFideliteTamponSelected: any;
  model = new User();
  dataUserEnseigne = new User();
  dataGpsPointDeVente = new User();

  // dataUserEnseigne ;
  // dataGpsPointDeVente ;
  datasCommune;
  latitutdeGPS = 20;
  longititudeGPS = 90;

  searchedLocation: Location = new Location(
    this.latitutdeGPS,
    this.longititudeGPS
  );
  pointDeVenteId: any;

  isSubmitted = false;
  actionSubmit = environment.CREER;
  //datasRole;

  // ************* end declaration des variables      *************

  ngOnInit() {
    this.initValueRadioInModel();

    this.pointDeVenteId = this.route.snapshot.params[environment.id];

    if (this.pointDeVenteId) {
      this.actionSubmit = environment.MODIFIER;

      // different de null
      // recuperer l'enseigne en question
      // récupération de la liste des roles
      let data = new User();
      data.id = this.pointDeVenteId;
      this.metiersService
        .getByCriteria(environment.pointDeVente, data)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;

            if (
              response != null &&
              !response.hasError &&
              response.items != null
            ) {
              this.model = response.items[0];

              // setter les datas pour la maj
              if (
                this.utilitiesService.isNotEmpty(this.model.datasUserEnseigne)
              ) {
                this.dataUserEnseigne = this.model.datasUserEnseigne[0];
              }
              this.dataGpsPointDeVente = this.model.dataGpsPointDeVente;
              this.datasProgrammeFideliteCarteSelected =
                this.model.datasProgrammeFideliteCarte;
              this.datasProgrammeFideliteTamponSelected =
                this.model.datasProgrammeFideliteTampon;
              this.initSearchedLocation();
              // update radio
              if (this.model.isAllTamponDipso) {
                this.radioTampon = "" + this.model.isAllTamponDipso;
              }
              if (this.model.isAllCarteDipso) {
                this.radioCarte = "" + this.model.isAllCarteDipso;
              }
            }
          },
          (err) => {
            console.log(err);
          }
        );
    } else {
      // this.optionsRadio[0].checked = false;
      // this.optionsRadio[1].checked = true;
      this.initSearchedLocation();
    }

    // get datasProgrammeFideliteCarte/datasProgrammeFideliteTampon
    let enseigneId = this.utilitiesService.enseigneId;
    if (enseigneId) {
      let data = new User();
      data.enseigneId = enseigneId;

      this.metiersService
        .getByCriteria(environment.programmeFideliteCarte, data)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            if (
              response != null &&
              !response.hasError &&
              response.items != null
            ) {
              this.datasProgrammeFideliteCarte = response.items;
            }
          },
          (err) => {
            console.log(err);
          }
        );

      this.metiersService
        .getByCriteria(environment.programmeFideliteTampon, data)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            if (
              response != null &&
              !response.hasError &&
              response.items != null
            ) {
              this.datasProgrammeFideliteTampon = response.items;
            }
          },
          (err) => {
            console.log(err);
          }
        );
    }

    // récupération de la liste des communes
    this.metiersService.getByCriteria(environment.commune).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;
        console.log("response commune : ", response);

        if (response != null && !response.hasError && response.items != null) {
          this.datasCommune = response.items;
        }
      },
      (err) => {
        console.log(err);
      }
    );

    // récupération de la liste des roles
    // let data = new User();
    // data.code = environment.ADMINISTRATEUR;
    // data.codeParam = { operator: "<>" };

    // let dataRoleEnseigne = new User();
    // dataRoleEnseigne.code = environment.UTILISATEUR_ENSEIGNE;
    // dataRoleEnseigne.codeParam = { operator: "<>" };
    // let dataRoleMobile = new User();
    // dataRoleMobile.code = environment.UTILISATEUR_MOBILE;
    // dataRoleMobile.codeParam = { operator: "<>" };

    // let datas = [];
    // datas.push(dataRoleEnseigne);
    // datas.push(dataRoleMobile);

    // this.metiersService
    //   .getByCriteria(environment.role, data, datas, true)
    //   .subscribe(
    //     (res) => {
    //       let response = new Response<any>();
    //       response = res;
    //       console.log("response role : ", response);

    //       if (
    //         response != null &&
    //         !response.hasError &&
    //         response.items != null
    //       ) {
    //         this.datasRole = response.items;
    //       }
    //     },
    //     (err) => {
    //       console.log(err);
    //     }
    //   );
  }

  submit(monModelForm: NgForm) {
    this.isSubmitted = true; // disable button submit
    this.utilitiesService.begin("submit");
    console.log("monModelForm.value : ", monModelForm.value);

    // ajout des id, data et datas au model
    this.model.enseigneId = this.utilitiesService.enseigneId;
    if (this.dataUserEnseigne.email) {
      this.model.datasUserEnseigne = [this.dataUserEnseigne];
    }
    if (this.dataGpsPointDeVente.latitude && this.dataGpsPointDeVente.longititude) {
      this.model.dataGpsPointDeVente = this.dataGpsPointDeVente;
    }
    if (
      this.utilitiesService.isNotEmpty(this.datasProgrammeFideliteCarteSelected)
    ) {
      this.model.datasProgrammeFideliteCarte =
        this.datasProgrammeFideliteCarteSelected;
    }
    if (
      this.utilitiesService.isNotEmpty(
        this.datasProgrammeFideliteTamponSelected
      )
    ) {
      this.model.datasProgrammeFideliteTampon =
        this.datasProgrammeFideliteTamponSelected;
    }

    console.log("this.model : ", this.model);

    // appel service
    this.metiersService
      .edit(
        environment.pointDeVente +
          (this.model.id ? environment.update : environment.create),
        [this.model]
      )
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );

            console.log(
              "localStorage.getItem(environment.pointDeVenteId)",
              localStorage.getItem(environment.pointDeVenteId)
            );

            console.log(
              "this.utilitiesService.pointDeVenteId",
              this.utilitiesService.pointDeVenteId
            );

            if (
              !this.utilitiesService.pointDeVenteId ||
              this.utilitiesService.pointDeVenteId.length == 0
            ) {
              console.log("begin if");
              localStorage.setItem(
                environment.pointDeVenteId,
                response.items[0].id
              );
            } else {
              console.log("begin else");
            }
            // navigation retour a la liste des tampons
            this.router.navigateByUrl(
              environment.pages + environment.CPDVEnseigne
            );

            // reset form
            this.utilitiesService.resetForm(monModelForm);
            this.resetDatas();
            // message retour
          } else {
            if (response != null) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusDanger
              );
            }
            // activer le button du formulaire
            this.isSubmitted = false;
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
          this.isSubmitted = false;
        }
      );

    //this.isSubmitted = false; // enable button submit
    this.utilitiesService.end("submit");
  }

  changeIsAllTamponDipsoValue(event) {
    this.utilitiesService.begin("changeIsAllTamponDipsoValue");
    this.model.isAllTamponDipso = event;
    //this.model.isAllTamponDipso = JSON.parse(event);
    this.radioTampon = event;
  }

  initValueRadioInModel() {
    this.utilitiesService.begin("initValueRadioInModel");

    if (!this.model.isAllCarteDipso) {
      this.model.isAllCarteDipso = this.radioCarte;
      console.log("this.model.isAllCarteDipso : ", this.model.isAllCarteDipso);
    }
    if (!this.model.isAllTamponDipso) {
      this.model.isAllTamponDipso = this.radioTampon;
      console.log(
        "this.model.isAllTamponDipso : ",
        this.model.isAllTamponDipso
      );
    }
  }

  changeIsAllCarteDipsoValue(event) {
    this.utilitiesService.begin("changeIsAllCarteDipsoValue");
    this.model.isAllCarteDipso = event;
    //this.model.isAllCarteDipso = JSON.parse(event);
    this.radioCarte = event;
  }

  changeValueRadioAjouterUser(event) {
    this.utilitiesService.begin("changeValueRadioAjouterUser");
    console.log("event ", event);
    this.radioAjouterUser = event;
    //this.radioAjouterUser = JSON.parse(event);
    console.log("event after ", JSON.parse(event));
  }

  updateLocation() {
    //updateLocation(event: Location) {
    this.utilitiesService.begin("updateLocation");

    // this.searchedLocation = new Location(event.latitude, event.longitude);
  }
  onKeyLongititude(event) {
    this.utilitiesService.begin("onKeyLongititude");
    console.log("event ", event.target.value);
    this.longititudeGPS = event.target.value;
    this.searchedLocation = new Location(
      this.latitutdeGPS,
      this.longititudeGPS
    );
  }

  onKeyLatitude(event) {
    this.utilitiesService.begin("onKeyLatitude");
    console.log("event ", event.target.value);
    this.latitutdeGPS = event.target.value;
    this.searchedLocation = new Location(
      this.latitutdeGPS,
      this.longititudeGPS
    );
  }

  initSearchedLocation() {
    if (
      this.dataGpsPointDeVente.latitude ||
      this.dataGpsPointDeVente.longititude
    ) {
      this.searchedLocation = new Location(
        this.dataGpsPointDeVente.latitude,
        this.dataGpsPointDeVente.longititude
      );
    } else {
      this.dataGpsPointDeVente.latitude = this.searchedLocation.latitude;
      this.dataGpsPointDeVente.longititude = this.searchedLocation.longitude;
    }
  }

  resetDatas() {
    this.model = new User();
    this.dataGpsPointDeVente = new User();
    this.dataUserEnseigne= new User();
    this.datasProgrammeFideliteCarteSelected = [];
    this.datasProgrammeFideliteTamponSelected = [];
  }
}
