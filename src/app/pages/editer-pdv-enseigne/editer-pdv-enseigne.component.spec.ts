import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditerPdvEnseigneComponent } from './editer-pdv-enseigne.component';

describe('EditerPdvEnseigneComponent', () => {
  let component: EditerPdvEnseigneComponent;
  let fixture: ComponentFixture<EditerPdvEnseigneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditerPdvEnseigneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditerPdvEnseigneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
