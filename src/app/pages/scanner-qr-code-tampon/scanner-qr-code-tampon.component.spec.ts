import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannerQrCodeTamponComponent } from './scanner-qr-code-tampon.component';

describe('ScannerQrCodeTamponComponent', () => {
  let component: ScannerQrCodeTamponComponent;
  let fixture: ComponentFixture<ScannerQrCodeTamponComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScannerQrCodeTamponComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScannerQrCodeTamponComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
