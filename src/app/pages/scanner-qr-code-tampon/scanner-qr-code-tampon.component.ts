import { environment } from "./../../../environments/environment";
import { Component, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { QrScannerComponent } from "angular2-qrscanner";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";
import { Response } from "../../models/response";

@Component({
  selector: "ngx-scanner-qr-code-tampon",
  templateUrl: "./scanner-qr-code-tampon.component.html",
  styleUrls: ["./scanner-qr-code-tampon.component.scss"],
})
export class ScannerQrCodeTamponComponent implements OnInit {
  constructor(
    private router: Router,
    private metiersService: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}

  description;
  isDebiter: boolean;
  isVisibleScan: boolean;
  @ViewChild(QrScannerComponent, { static: false })
  qrScannerComponent: QrScannerComponent;

  ngOnInit() {}

  ngAfterViewInit() {
    this.qrScannerComponent.getMediaDevices().then((devices) => {
      console.log("devices : ", devices);
      const videoDevices: MediaDeviceInfo[] = [];
      for (const device of devices) {
        if (device.kind.toString() === "videoinput") {
          videoDevices.push(device);
        }
      }
      if (videoDevices.length > 0) {
        let choosenDev;
        for (const dev of videoDevices) {
          if (dev.label.includes("front")) {
            choosenDev = dev;
            break;
          }
        }
        if (choosenDev) {
          this.qrScannerComponent.chooseCamera.next(choosenDev);
        } else {
          this.qrScannerComponent.chooseCamera.next(videoDevices[0]);
        }
      }
    });

    this.qrScannerComponent.capturedQr.subscribe((result) => {
      console.log("result : ", result);
      //this.router.navigateByUrl(environment.pageAccueil);

      let infosQrCode = result.split(" ");
      let data = new User();

      data.id = infosQrCode[0];
      data.programmeFideliteTamponId = infosQrCode[1];
      data.pointDeVenteId = this.utilitiesService.pointDeVenteId;

      data.description = this.description;
      //data.typeActionCode = this.dataTypeAction.code;
      //data.somme =infosQrCode.length > 2 && infosQrCode[2] != null? infosQrCode[2] : this.somme;
      //data.contenuScanner = result;

      // appel service
      this.metiersService
        .edit(environment.carte + environment.flasherTampon, [data])
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            if (response != null && !response.hasError) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusSuccess
              );
              // navigation retour a la liste des tampons
              this.router.navigateByUrl(
                environment.pages + environment.CMesClients
              );

              // reset form
              //this.resetDatas();
              // message retour
            } else {
              if (response != null) {
                // notification
                this.utilitiesService.showToast(
                  response.status.message,
                  this.utilitiesService.statusDanger
                );
              }
            }
          },
          (err) => {
            console.log(err);
            this.utilitiesService.showToast(
              environment.erreurDeConnexion,
              this.utilitiesService.statusInfo
            );
          }
        );
    });
  }
}
