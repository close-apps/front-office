import { Component, OnInit } from "@angular/core";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { SendDatasService } from "../../services/send-datas.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-details-historique-client",
  templateUrl: "./details-historique-client.component.html",
  styleUrls: ["./details-historique-client.component.scss"],
})
export class DetailsHistoriqueClientComponent implements OnInit {
  itemsHistoriqueCarte;
  itemsHistoriqueTampon;
  user;
  constructor(
    private metiersService: MetiersService,
    private utilities: UtilitiesService,
    private sendDatasService: SendDatasService
  ) {}

  ngOnInit() {
    this.initDatas();
  }

  initDatas() {
    let data = new User();
    if (this.sendDatasService.data) {
      this.user = this.sendDatasService.data;
      data.carteId = this.sendDatasService.data.id;
      this.metiersService
        .getByCriteria(environment.historiqueCarteProgrammeFideliteCarte, data)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            console.log("response : ", response);
            if (response != null && !response.hasError) {
              this.itemsHistoriqueCarte = response.items;
            }
          },
          (err) => {
            console.log(err);
            this.utilities.showToast(
              environment.erreurDeConnexion,
              this.utilities.statusInfo
            );
          }
        );
      this.metiersService
        .getByCriteria(environment.historiqueCarteProgrammeFideliteTampon, data)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            console.log("response : ", response);
            if (response != null && !response.hasError) {
              this.itemsHistoriqueTampon = response.items;
            }
          },
          (err) => {
            console.log(err);
            this.utilities.showToast(
              environment.erreurDeConnexion,
              this.utilities.statusInfo
            );
          }
        );
    }
  }
}
