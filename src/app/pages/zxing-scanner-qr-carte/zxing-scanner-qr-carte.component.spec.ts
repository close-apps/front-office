import { async, ComponentFixture, TestBed } from "@angular/core/testing";

import { ZxingScannerQrCarteComponent } from "./zxing-scanner-qr-carte.component";

describe("ZxingScannerQrCarteComponent", () => {
  let component: ZxingScannerQrCarteComponent;
  let fixture: ComponentFixture<ZxingScannerQrCarteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ZxingScannerQrCarteComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZxingScannerQrCarteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it("should create", () => {
    expect(component).toBeTruthy();
  });
});
