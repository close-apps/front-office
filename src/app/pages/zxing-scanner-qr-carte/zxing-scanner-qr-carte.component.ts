import { ViewChild } from "@angular/core";
import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ZXingScannerComponent } from "@zxing/ngx-scanner";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-zxing-scanner-qr-carte",
  templateUrl: "./zxing-scanner-qr-carte.component.html",
  styleUrls: ["./zxing-scanner-qr-carte.component.scss"],
})
export class ZxingScannerQrCarteComponent implements OnInit {
  @ViewChild("scanner", { static: false })
  scanner: ZXingScannerComponent;

  desiredDevice;
  scannerEnabled = true;
  messageAttenteApresScan = environment.messageAttenteApresScan;

  isActiverScan = false;
  isScannerOk = false;
  torch;
  libelleActionScan = environment.afficherScannerQrCode;

  // datasTypeAction = [
  //   { code: "DEBITER", libelle: "DEBITER" },
  //   { code: "CREDITER", libelle: "CREDITER" },
  // ];
  datasTypeAction = environment.datasDebitCredit;
  somme;
  description;
  dataTypeAction;
  isVisibleScan = false;
  isDebiter = true;
  constructor(
    private router: Router,
    private metiersService: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}

  ngOnInit() {
    this.dataTypeAction = this.datasTypeAction[0];
  }
  camerasNotFoundHandler(event) {
    this.utilitiesService.begin("camerasNotFoundHandler");
    console.log("event :::: ", event);
  }
  scanSuccessHandler(event) {
    this.utilitiesService.begin("scanSuccessHandler");
    console.log("event :::: ", event);
    this.scannerEnabled = false;
    this.isScannerOk = true;
    this.sendScan(event);
  }
  camerasFoundHandler(event) {
    this.utilitiesService.begin("camerasFoundHandler");
    console.log("event :::: ", event);
  }
  onTorchCompatible(event) {
    this.utilitiesService.begin("onTorchCompatible");
    console.log("event :::: ", event);
  }
  scanCompleteHandler(event) {
    this.utilitiesService.begin("scanCompleteHandler");
    console.log("event :::: ", event);
  }
  scanFailureHandler(event) {
    this.utilitiesService.begin("scanFailureHandler");
    console.log("event :::: ", event);
  }
  scanErrorHandler(event) {
    this.utilitiesService.begin("scanErrorHandler");
    console.log("event :::: ", event);
  }

  activerScan(value) {
    this.isActiverScan = value;

    if (value) {
      this.libelleActionScan = environment.cacherScannerQrCode;
    } else {
      this.libelleActionScan = environment.afficherScannerQrCode;
    }
  }
  /**
   * Some method.
   */
  doSomething(): void {
    //this.scanner.device = this.getBackCamera();
  }

  /**
   * Returns the back camera for ya.
   */
  getBackCamera() {
    //return theBackCamera;
  }

  changeAction(event) {
    console.log("changeAction event: ", event);
    console.log("changeAction event.code: ", event.code);
    if (event && event.code != environment.CODEDEBITER) {
      this.isDebiter = false;
    } else {
      this.isDebiter = true;
    }
  }

  onKeySomme(event) {
    console.log("onKeySomme event: ", event);
    this.isVisibleScan = true;
  }
  resetDatas() {
    this.somme = null;
    this.dataTypeAction = null;
  }

  sendScan(resultScan = "") {
    console.log("resultScan ::: ", resultScan);

    let infosQrCode = resultScan.split(" ");
    let data = new User();
    console.log("infosQrCode ::: ", infosQrCode);

    if (infosQrCode[0]) {
      data.id = infosQrCode[0];
    }
    if (infosQrCode[1]) {
      data.programmeFideliteCarteId = infosQrCode[1];
    }
    data.pointDeVenteId = this.utilitiesService.pointDeVenteId;
    data.typeActionCode = this.dataTypeAction.code;
    data.somme =
      infosQrCode.length > 2 && infosQrCode[2] != null
        ? infosQrCode[2]
        : this.somme;
    data.chiffreDaffaire = this.somme;
    data.description = this.description;

    // appel service
    this.metiersService
      .edit(environment.carte + environment.flasherCarte, [data])
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            // navigation retour a la liste des tampons
            this.router.navigateByUrl(
              environment.pages + environment.CMesClients
            );

            // reset form
            this.resetDatas();
            // message retour
          } else {
            if (response != null && response.hasError) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusDanger
              );
              this.initScanner();
            }
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexionOuDataIncorrect,
            this.utilitiesService.statusDanger
          );
          this.initScanner();
        }
      );
  }

  initScanner() {
    this.utilitiesService.begin("initScanner");
    this.isScannerOk = false;
    this.scannerEnabled = true;
    //this.scanner.reset();
    //this.scanner.restart();
  }
}
