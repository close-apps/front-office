import { ProComponent } from "./pro/pro.component";
import { ConfidentialiteComponent } from "./confidentialite/confidentialite.component";
import { NousContacterComponent } from "./nous-contacter/nous-contacter.component";
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";

import { PagesComponent } from "./pages.component";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { ECommerceComponent } from "./e-commerce/e-commerce.component";
import { NotFoundComponent } from "./miscellaneous/not-found/not-found.component";
import { TokenInvalideComponent } from "./token-invalide/token-invalide.component";
import { MajProfilComponent } from "./maj-profil-enseigne/maj-profil.component";
import { MajProfilUserComponent } from "./maj-profil-user/maj-profil-user.component";
import { EditerPdftamponEnseigneComponent } from "./editer-pdftampon-enseigne/editer-pdftampon-enseigne.component";
import { EditerPdfcarteEnseigneComponent } from "./editer-pdfcarte-enseigne/editer-pdfcarte-enseigne.component";
import { PdftamponEnseigneComponent } from "./pdftampon-enseigne/pdftampon-enseigne.component";
import { PdfcarteEnseigneComponent } from "./pdfcarte-enseigne/pdfcarte-enseigne.component";
import { PdvEnseigneComponent } from "./pdv-enseigne/pdv-enseigne.component";
import { EditerPdvEnseigneComponent } from "./editer-pdv-enseigne/editer-pdv-enseigne.component";
import { ScannerQrCodeComponent } from "./scanner-qr-code/scanner-qr-code.component";
import { MesClientsComponent } from "./mes-clients/mes-clients.component";
import { DetailsClientComponent } from "./details-client/details-client.component";
import { ScannerQrCodeTamponComponent } from "./scanner-qr-code-tampon/scanner-qr-code-tampon.component";
import { AccueilComponent } from "./accueil/accueil.component";
import { ZxingScannerQrCarteComponent } from "./zxing-scanner-qr-carte/zxing-scanner-qr-carte.component";
import { ZxingScannerQrTamponComponent } from "./zxing-scanner-qr-tampon/zxing-scanner-qr-tampon.component";
import { ScannerManuellementComponent } from "./scanner-manuellement/scanner-manuellement.component";
import { ScannerManuellementParProgrammeComponent } from "./scanner-manuellement-par-programme/scanner-manuellement-par-programme.component";
import { DetailsHistoriqueClientComponent } from "./details-historique-client/details-historique-client.component";
import { TableauDeBordComponent } from "./tableau-de-bord/tableau-de-bord.component";
import { IsConnectedService } from "../services/is-connected.service";

const routes: Routes = [
  {
    path: "",
    component: PagesComponent,
    children: [
      // {
      //   path: "accueil",
      //   //component: ECommerceComponent,
      //   component: AccueilComponent,
      // },

      {
        path: "accueil",
        component: AccueilComponent,
        canActivate: [IsConnectedService], // TODO: a securiser
      },
      {
        path: "tableau-de-bord",
        //component: ECommerceComponent,
        component: TableauDeBordComponent,
      },
      {
        path: "nous-contacter",
        component: NousContacterComponent,
      },
      {
        path: "confidentialite",
        component: ConfidentialiteComponent,
      },
      {
        path: "pro",
        component: ProComponent,
      },

      {
        path: "pdv-enseigne",
        component: PdvEnseigneComponent,
      },
      {
        path: "pdv-enseigne/:id",
        component: PdvEnseigneComponent,
      },
      {
        path: "editer-pdv-enseigne",
        component: EditerPdvEnseigneComponent,
      },
      {
        path: "editer-pdv-enseigne/:id",
        component: EditerPdvEnseigneComponent,
      },
      {
        path: "pdftampon-enseigne",
        component: PdftamponEnseigneComponent,
      },
      {
        path: "pdftampon-enseigne/:id",
        component: PdftamponEnseigneComponent,
      },
      {
        path: "editer-pdftampon-enseigne",
        component: EditerPdftamponEnseigneComponent,
      },
      {
        path: "editer-pdftampon-enseigne/:id",
        component: EditerPdftamponEnseigneComponent,
      },
      {
        path: "pdfcarte-enseigne",
        component: PdfcarteEnseigneComponent,
      },

      {
        path: "pdfcarte-enseigne/:id",
        component: PdfcarteEnseigneComponent,
      },
      {
        path: "editer-pdfcarte-enseigne",
        component: EditerPdfcarteEnseigneComponent,
      },
      {
        path: "editer-pdfcarte-enseigne/:id",
        component: EditerPdfcarteEnseigneComponent,
      },
      {
        path: "maj-profil-enseigne/:id",
        component: MajProfilComponent,
      },
      {
        path: "maj-profil-user/:id",
        component: MajProfilUserComponent,
      },
      {
        path: "token-invalide",
        component: TokenInvalideComponent,
      },
      // {
      //   path: "scanner-qr-code",
      //   component: ScannerQrCodeComponent,
      // },
      // {
      //   path: "scanner-tampon",
      //   component: ScannerQrCodeTamponComponent,
      // },
      {
        path: "scanner-manuellement",
        component: ScannerManuellementComponent,
      },
      {
        path: "scanner-manuellement-par-programme",
        component: ScannerManuellementParProgrammeComponent,
      },

      {
        path: "scanner-qr-code",
        component: ZxingScannerQrCarteComponent,
      },
      {
        path: "scanner-tampon",
        component: ZxingScannerQrTamponComponent,
      },
      {
        path: "mes-clients",
        component: MesClientsComponent,
      },
      {
        path: "details-client",
        component: DetailsClientComponent,
      },
      {
        path: "details-historique-client",
        component: DetailsHistoriqueClientComponent,
      },
      {
        path: "iot-dashboard",
        component: DashboardComponent,
      },
      {
        path: "layout",
        loadChildren: () =>
          import("./layout/layout.module").then((m) => m.LayoutModule),
      },
      {
        path: "forms",
        loadChildren: () =>
          import("./forms/forms.module").then((m) => m.FormsModule),
      },
      {
        path: "ui-features",
        loadChildren: () =>
          import("./ui-features/ui-features.module").then(
            (m) => m.UiFeaturesModule
          ),
      },
      {
        path: "modal-overlays",
        loadChildren: () =>
          import("./modal-overlays/modal-overlays.module").then(
            (m) => m.ModalOverlaysModule
          ),
      },
      {
        path: "extra-components",
        loadChildren: () =>
          import("./extra-components/extra-components.module").then(
            (m) => m.ExtraComponentsModule
          ),
      },
      {
        path: "maps",
        loadChildren: () =>
          import("./maps/maps.module").then((m) => m.MapsModule),
      },
      {
        path: "charts",
        loadChildren: () =>
          import("./charts/charts.module").then((m) => m.ChartsModule),
      },
      {
        path: "editors",
        loadChildren: () =>
          import("./editors/editors.module").then((m) => m.EditorsModule),
      },
      {
        path: "tables",
        loadChildren: () =>
          import("./tables/tables.module").then((m) => m.TablesModule),
      },
      {
        path: "miscellaneous",
        loadChildren: () =>
          import("./miscellaneous/miscellaneous.module").then(
            (m) => m.MiscellaneousModule
          ),
      },
      {
        path: "",
        redirectTo: "accueil",
        pathMatch: "full",
      },
      {
        path: "**",
        component: NotFoundComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
