import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { NbDialogService } from "@nebular/theme";
import { FindValueSubscriber } from "rxjs/internal/operators/find";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { SendDatasService } from "../../services/send-datas.service";
import { UtilitiesService } from "../../services/utilities.service";
import { DialogNamePromptCustomComponent } from "../dialog-name-prompt-custom/dialog-name-prompt-custom.component";
import { DialogNamePromptDebloquerComponent } from "../dialog-name-prompt-debloquer/dialog-name-prompt-debloquer.component";
import { DialogNamePromptComponent } from "../modal-overlays/dialog/dialog-name-prompt/dialog-name-prompt.component";

@Component({
  selector: "ngx-details-client",
  templateUrl: "./details-client.component.html",
  styleUrls: ["./details-client.component.scss"],
})
export class DetailsClientComponent implements OnInit {
  carte;
  itemsPDV;
  itemsPDVProgrammeFideliteCarte;
  itemsPDVProgrammeFideliteTampon;
  itemsDelitProgrammeFidelite;
  user;
  itemsHistoriqueCarte;
  itemsHistoriqueTampon;
  // datasTypeAction = [
  //   { code: "DEBITER", libelle: "DEBITER" },
  //   { code: "CREDITER", libelle: "CREDITER" },
  // ];
  datasTypeAction = environment.datasDebitCredit;

  dataTypeAction;
  sommeCarte;

  descriptionCarte;
  descriptionTampon;

  souscriptionTampon;
  souscriptionCarte;
  constructor(
    public sendDatasService: SendDatasService,
    private utilitiesService: UtilitiesService,
    private metiersService: MetiersService,
    private router: Router,
    private dialogService: NbDialogService
  ) {}

  ngOnInit() {
    this.getDatas();
  }

  changeIsDispoInAllPdvValue(event) {
    this.utilitiesService.begin("changeIsDispoInAllPdvValue");
  }

  getDatas() {
    if (this.sendDatasService.data && this.sendDatasService.data.pdfId) {
      this.user = this.sendDatasService.data;

      let dataCarte = new User();
      dataCarte.userId = this.sendDatasService.data.id;
      if (!this.utilitiesService.valueIsAdminEnseigne) {
        dataCarte.pointDeVenteId = this.utilitiesService.pointDeVenteId;
        if (this.sendDatasService.isPDFTampon) {
          dataCarte.programmeFideliteTamponId =
            this.sendDatasService.data.pdfId;
        } else {
          dataCarte.programmeFideliteCarteId = this.sendDatasService.data.pdfId;
        }
      }
      this.metiersService
        .getByCriteria(environment.delitProgrammeFidelite, dataCarte)
        //.getByCriteria(environment.user, data)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            console.log("response : ", response);
            if (response != null && !response.hasError) {
              if (response.items && response.items.length > 0) {
                this.itemsDelitProgrammeFidelite = response.items;
                //this.user = this.itemsDelitProgrammeFidelite[0];
              }
            }
          },
          (err) => {
            console.log(err);
            this.utilitiesService.showToast(
              environment.erreurDeConnexion,
              this.utilitiesService.statusInfo
            );
          }
        );

      this.metiersService
        .getByCriteria(
          environment.historiqueCarteProgrammeFideliteCarte,
          dataCarte
        )
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            console.log("response : ", response);
            if (response != null && !response.hasError) {
              this.itemsHistoriqueCarte = response.items;
            }
          },
          (err) => {
            console.log(err);
            this.utilitiesService.showToast(
              environment.erreurDeConnexion,
              this.utilitiesService.statusInfo
            );
          }
        );
      this.metiersService
        .getByCriteria(
          environment.historiqueCarteProgrammeFideliteTampon,
          dataCarte
        )
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            console.log("response : ", response);
            if (response != null && !response.hasError) {
              this.itemsHistoriqueTampon = response.items;
            }
          },
          (err) => {
            console.log(err);
            this.utilitiesService.showToast(
              environment.erreurDeConnexion,
              this.utilitiesService.statusInfo
            );
          }
        );
    }
  }

  get getNameUser() {
    let name = this.sendDatasService.data.userLogin;
    if (this.sendDatasService.data.userNom) {
      name =
        this.sendDatasService.data.userNom +
        " " +
        this.sendDatasService.data.userPrenoms;
    }

    return name;
  }

  confirmation() {
    this.dialogService
      .open(DialogNamePromptComponent)
      .onClose.subscribe((rep) => {
        if (rep && rep == "OUI") {
        }
      });
  }
  notifierUserEnDelit(dataUser) {
    this.dialogService
      .open(DialogNamePromptCustomComponent)
      .onClose.subscribe((rep) => {
        if (rep) {
          dataUser.isNotifed = true;
          dataUser.messageNotifed = rep;
          this.metiersService
            .update(environment.delitProgrammeFidelite, [dataUser])
            //.getByCriteria(environment.user, data)
            .subscribe(
              (res) => {
                let response = new Response<any>();
                response = res;
                console.log("response : ", response);
                if (response != null) {
                  if (!response.hasError) {
                    this.utilitiesService.showToast(
                      response.status.message,
                      this.utilitiesService.statusSuccess
                    );
                    if (response.items && response.items.length > 0) {
                      dataUser = response.items[0];
                    }
                  } else {
                    this.utilitiesService.showToast(
                      response.status.message,
                      this.utilitiesService.statusInfo
                    );
                  }
                }
              },
              (err) => {
                console.log(err);
                this.utilitiesService.showToast(
                  environment.erreurDeConnexion,
                  this.utilitiesService.statusInfo
                );
              }
            );
        }
      });
  }
  bloquerUserEnDelit(dataUser) {
    this.dialogService
      .open(DialogNamePromptComponent)
      .onClose.subscribe((rep) => {
        console.log("bloquerUserEnDelit :::: ", rep);
        if (rep) {
          // mettre la logique de blocage d'un user

          dataUser.isBlocageUserInPDFAndPDV = true;
          dataUser.isApplicateSanction = true;

          dataUser.messageLocked = rep;
          this.metiersService
            .update(environment.delitProgrammeFidelite, [dataUser])
            //.getByCriteria(environment.user, data)
            .subscribe(
              (res) => {
                let response = new Response<any>();
                response = res;
                console.log("response : ", response);
                if (response != null) {
                  if (!response.hasError) {
                    this.utilitiesService.showToast(
                      response.status.message,
                      this.utilitiesService.statusSuccess
                    );
                  } else {
                    this.utilitiesService.showToast(
                      response.status.message,
                      this.utilitiesService.statusInfo
                    );
                  }
                }
              },
              (err) => {
                console.log(err);
                this.utilitiesService.showToast(
                  environment.erreurDeConnexion,
                  this.utilitiesService.statusInfo
                );
              }
            );
        }
      });
  }
  debloquerUserEnDelit(dataUser) {
    this.dialogService
      .open(DialogNamePromptDebloquerComponent)
      .onClose.subscribe((rep) => {
        console.log("bloquerUserEnDelit :::: ", rep);
        if (rep && rep == "OUI") {
          dataUser.isBlocageUserInPDFAndPDV = false;
          dataUser.isApplicateSanction = false;
          this.metiersService
            .update(environment.delitProgrammeFidelite, [dataUser])
            //.getByCriteria(environment.user, data)
            .subscribe(
              (res) => {
                let response = new Response<any>();
                response = res;
                console.log("response : ", response);
                if (response != null) {
                  if (!response.hasError) {
                    this.utilitiesService.showToast(
                      response.status.message,
                      this.utilitiesService.statusSuccess
                    );
                  } else {
                    this.utilitiesService.showToast(
                      response.status.message,
                      this.utilitiesService.statusInfo
                    );
                  }
                }
              },
              (err) => {
                console.log(err);
                this.utilitiesService.showToast(
                  environment.erreurDeConnexion,
                  this.utilitiesService.statusInfo
                );
              }
            );
        }
      });
  }
}
