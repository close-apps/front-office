import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogNamePromptCustomComponent } from './dialog-name-prompt-custom.component';

describe('DialogNamePromptCustomComponent', () => {
  let component: DialogNamePromptCustomComponent;
  let fixture: ComponentFixture<DialogNamePromptCustomComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogNamePromptCustomComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogNamePromptCustomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
