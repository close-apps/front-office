import { Component, OnInit } from "@angular/core";
import { NbDialogRef } from "@nebular/theme";

@Component({
  selector: "ngx-dialog-name-prompt-custom",
  templateUrl: "./dialog-name-prompt-custom.component.html",
  styleUrls: ["./dialog-name-prompt-custom.component.scss"],
})
export class DialogNamePromptCustomComponent implements OnInit {
  constructor(protected ref: NbDialogRef<DialogNamePromptCustomComponent>) {}

  titre;
  DialogNamePromptComponent(titre) {}
  cancel() {
    console.log("cancel");
    //this.cancelEventEmitter.emit("cancel");
    this.ref.close();
  }

  submit(rep) {
    console.log("submit");
    //this.cancelEventEmitter.emit("submit");
    this.ref.close(rep);
  }

  ngOnInit() {}
}
