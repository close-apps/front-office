import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";
import { Location } from "../maps/search-map/entity/Location";

@Component({
  selector: "ngx-pdv-enseigne",
  templateUrl: "./pdv-enseigne.component.html",
  styleUrls: ["./pdv-enseigne.component.scss"],
})
export class PdvEnseigneComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private utilitiesService: UtilitiesService,
    private metiersService: MetiersService
  ) {}

  // ************* begin declaration des variables  *************
  model = new User();
  //datasPointDeVente: any;
  datasPDV: any;
  pointDeVenteId: any;
  enseigneId: any;
  optionAffichage = environment.ACCUEIL;

  dataUserEnseigne = new User();
  dataGpsPointDeVente = new User();

  aucunUserPourCePDV = environment.aucunUserPourCePDV;
  aucunGPSPourCePDV = environment.aucunGPSPourCePDV;
  donneeNonRenseigne = environment.donneeNonRenseigne;
  allPDFTamponPourCePDV = environment.allPDFTamponPourCePDV;
  allPDFCartePourCePDV = environment.allPDFCartePourCePDV;
  searchedLocation: Location;
  //datasInformationRegleSecuriteTampon: any; // pr la liste des regles de securite
  isAdminEnseigne : boolean;

  // ************* end declaration des variables    *************

  ngOnInit() {
    this.utilitiesService.begin("ngOnInit");

    this.isAdminEnseigne = this.utilitiesService.valueIsAdminEnseigne;

    this.pointDeVenteId = this.route.snapshot.params[environment.id];
    this.enseigneId = this.utilitiesService.enseigneId;
    if (this.pointDeVenteId && this.enseigneId) {
      this.optionAffichage = environment.VOIR;
      // different de null
      // recuperer l'enseigne en question
      // récupération de la liste des roles
      let data = new User();
      data.id = this.pointDeVenteId;
      data.enseigneId = this.enseigneId;
      // TODO:  se rassurer que le user est un admin
      this.metiersService
        .getByCriteria(environment.pointDeVente, data)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            console.log("response : ", response);
            if (response != null && !response.hasError) {
              this.model = response.items[0];

              if (
                this.utilitiesService.isNotEmpty(this.model.datasUserEnseigne)
              ) {
                this.dataUserEnseigne = this.model.datasUserEnseigne[0];
              }
              this.initSearchedLocation();
              //this.dataGpsPointDeVente = this.model.dataGpsPointDeVente;
            }
          },
          (err) => {
            console.log(err);
          }
        );
    } else {
      if (this.enseigneId) {
        // recuperer l'enseigne en question
        // récupération de la liste des roles
        let data = new User();
        //data.id = this.pointDeVenteId;
        if (!this.isAdminEnseigne) {
          data.userId = this.utilitiesService.userId;
        }
        data.enseigneId = this.enseigneId;
        // TODO:  se rassurer que le user est un admin
        this.metiersService
          .getByCriteria(environment.pointDeVente, data)
          .subscribe(
            (res) => {
              let response = new Response<any>();
              response = res;
              if (response != null && !response.hasError) {
                this.datasPDV = response.items;
              }
            },
            (err) => {
              console.log(err);
            }
          );
      } else {
        // merci de vous connectez
        this.utilitiesService.showToast(
          environment.merciDeVousConnectez,
          this.utilitiesService.statusInfo
        );
        // redirection
        this.router.navigateByUrl(environment.pageAccueil);
      }
    }
  }

  initSearchedLocation() {
    if (
      this.model.dataGpsPointDeVente &&
      this.model.dataGpsPointDeVente.latitude &&
      this.model.dataGpsPointDeVente.longititude
    ) {
      this.searchedLocation = new Location(
        this.dataGpsPointDeVente.latitude,
        this.dataGpsPointDeVente.longititude
      );
    }
  }

  showPDV(data) {
    this.router.navigateByUrl(
      environment.pages + environment.CPDVEnseigne + "/" + data.id
    );
  }
  updatePDV(data) {
    this.router.navigateByUrl(
      environment.pages + environment.CEditerPDVEnseigne + "/" + data.id
    );
  }
  createPDV(data) {
    this.router.navigateByUrl(
      environment.pages + environment.CEditerPDVEnseigne
    );
  }
  deletePDV(data) {
    this.utilitiesService.begin("deletePDV");

    // popup

    // delete
    this.metiersService.delete(environment.pointDeVente, [data]).subscribe(
      (res) => {
        let response = new Response<any>();
        response = res;
        if (response != null && !response.hasError) {
          this.utilitiesService.showToast(
            response.status.message,
            this.utilitiesService.statusSuccess
          );
          this.ngOnInit();
          this.router.navigateByUrl(
            environment.pages + environment.CPDVEnseigne
          );
        } else {
          this.utilitiesService.showToast(
            response.status.message,
            this.utilitiesService.statusDanger
          );
        }
      },
      (err) => {
        console.log(err);
        this.utilitiesService.showToast(
          environment.erreurDeConnexion,
          this.utilitiesService.statusInfo
        );
      }
    );
  }
}
