import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdvEnseigneComponent } from './pdv-enseigne.component';

describe('PdvEnseigneComponent', () => {
  let component: PdvEnseigneComponent;
  let fixture: ComponentFixture<PdvEnseigneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdvEnseigneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdvEnseigneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
