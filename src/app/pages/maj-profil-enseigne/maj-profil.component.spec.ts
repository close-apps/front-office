import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MajProfilComponent } from './maj-profil.component';

describe('MajProfilComponent', () => {
  let component: MajProfilComponent;
  let fixture: ComponentFixture<MajProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MajProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MajProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
