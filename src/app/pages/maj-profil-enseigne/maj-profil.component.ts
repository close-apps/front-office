import { Component, OnInit } from "@angular/core";
import { NgForm } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-maj-profil",
  templateUrl: "./maj-profil.component.html",
  styleUrls: ["./maj-profil.component.scss"],
})
export class MajProfilComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private metiersService: MetiersService,
    private utilitiesService: UtilitiesService
  ) {}
  showPassword = true;
  isSubmitted = false;
  enseigneId;

  model = new User();

  datasRole;

  datasEnseigne;

  ngOnInit() {
    this.enseigneId = this.route.snapshot.params[environment.id];
    if (this.enseigneId) {
      // recuperer l'enseigne en question
      // récupération de la liste des roles
      this.model.id = this.enseigneId;
      this.metiersService
        .getByCriteria(environment.enseigne, this.model)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            console.log("response enseigneId : ", response);

            if (
              response != null &&
              !response.hasError &&
              response.items != null
            ) {
              this.model = response.items[0];
            }
          },
          (err) => {
            console.log(err);
          }
        );
    } else {
    }

    // récupération de la liste des roles
    let data = new User();
    data.code = environment.ADMINISTRATEUR;
    data.codeParam = { operator: "<>" };

    let dataRoleEnseigne = new User();
    dataRoleEnseigne.code = environment.UTILISATEUR_ENSEIGNE;
    dataRoleEnseigne.codeParam = { operator: "<>" };
    let dataRoleMobile = new User();
    dataRoleMobile.code = environment.UTILISATEUR_MOBILE;
    dataRoleMobile.codeParam = { operator: "<>" };

    let datas = [];
    datas.push(dataRoleEnseigne);
    datas.push(dataRoleMobile);

    this.metiersService
      .getByCriteria(environment.role, data, datas, true)
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          console.log("response role : ", response);

          if (
            response != null &&
            !response.hasError &&
            response.items != null
          ) {
            this.datasRole = response.items;
          }
        },
        (err) => {
          console.log(err);
        }
      );
  }

  async submit(monModelForm: NgForm) {
    let response = new Response<any>();

    // desactiver le button du formulaire
    this.isSubmitted = monModelForm.submitted;
    console.log(monModelForm.value);

    // appel service
    this.metiersService.update(environment.enseigne, [this.model]).subscribe(
      (res) => {
        response = res;
        console.log("response submit :", response);
        if (response != null && !response.hasError) {
          // maj du localStorage
          //this.utilitiesService.connexionLocalStorage(response.items != null ? response.items[0] : null );

          // notification
          this.utilitiesService.showToast(
            response.status.message,
            this.utilitiesService.statusSuccess
          );
          // navigation etant connecter
          this.router.navigateByUrl(environment.pageAccueil);
          //this.router.navigateByUrl("../pages/dashboard");

          // reset form
          this.utilitiesService.resetForm(monModelForm);
        } else {
          if (response != null) {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusDanger
            );
          }
          // activer le button du formulaire
          this.isSubmitted = false;
        }
      },
      (err) => {
        console.log(err);
        this.utilitiesService.showToast(
          environment.erreurDeConnexion,
          this.utilitiesService.statusInfo
        );
        this.isSubmitted = false;
      }
    );
  }
}
