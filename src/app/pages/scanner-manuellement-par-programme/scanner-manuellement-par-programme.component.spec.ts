import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScannerManuellementParProgrammeComponent } from './scanner-manuellement-par-programme.component';

describe('ScannerManuellementParProgrammeComponent', () => {
  let component: ScannerManuellementParProgrammeComponent;
  let fixture: ComponentFixture<ScannerManuellementParProgrammeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScannerManuellementParProgrammeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScannerManuellementParProgrammeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
