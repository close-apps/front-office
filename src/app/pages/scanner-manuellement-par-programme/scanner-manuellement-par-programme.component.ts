import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { SendDatasService } from "../../services/send-datas.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-scanner-manuellement-par-programme",
  templateUrl: "./scanner-manuellement-par-programme.component.html",
  styleUrls: ["./scanner-manuellement-par-programme.component.scss"],
})
export class ScannerManuellementParProgrammeComponent implements OnInit {
  isDebiter = true;

  // datasTypeAction = [
  //   { code: "DEBITER", libelle: "DEBITER" },
  //   { code: "CREDITER", libelle: "CREDITER" },
  // ];
  datasTypeAction = environment.datasDebitCredit;

  //datasTypeAction;
  isSubmitted = false;
  login;
  somme;
  chiffreDaffaire;
  description;
  dataTypeAction;
  constructor(
    private router: Router,
    private utilitiesService: UtilitiesService,
    public sendDatasService: SendDatasService,
    private metiersService: MetiersService
  ) {
    this.dataTypeAction = this.datasTypeAction[0];
  }

  ngOnInit() {}

  changeAction(event) {
    console.log("changeAction event: ", event);
    console.log("changeAction event.code: ", event.code);
    if (event && event.code != environment.CODEDEBITER) {
      this.isDebiter = false;
    } else {
      this.isDebiter = true;
    }
  }

  submit(monModelForm) {
    this.utilitiesService.begin("submit");
    this.isSubmitted = true;

    let data = new User();

    data.userLogin = this.login;
    data.isScanManuel = true;
    data.programmeFideliteTamponId = this.sendDatasService.data.id;
    data.programmeFideliteCarteId = this.sendDatasService.data.id;
    data.pointDeVenteId = this.utilitiesService.pointDeVenteId;
    data.description = this.description;
    data.typeActionCode = this.dataTypeAction.code;

    if (this.isDebiter) {
      data.somme = this.somme;
      data.chiffreDaffaire = this.chiffreDaffaire;
    } else {
      data.somme = this.somme;
      data.chiffreDaffaire = this.somme;
    }

    // appel service
    this.metiersService
      .edit(
        this.sendDatasService.isPDFTampon
          ? environment.carte + environment.flasherTampon
          : environment.carte + environment.flasherCarte,
        [data]
      )
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            // notification
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            // navigation retour a la liste des tampons
            this.router.navigateByUrl(
              this.sendDatasService.isPDFTampon
                ? environment.pages + environment.CPDFTamponEnseigne
                : environment.pages + environment.CPDFCarteEnseigne
            );

            // reset form
            this.resetDatas();
            // message retour
          } else {
            if (response != null) {
              // notification
              this.utilitiesService.showToast(
                response.status.message,
                this.utilitiesService.statusDanger
              );
            }
            this.isSubmitted = false;
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
          this.isSubmitted = false;
        }
      );
  }

  resetDatas() {
    this.login = null;
    this.description = null;
    this.somme = null;
    this.dataTypeAction = this.datasTypeAction[0];
    this.isSubmitted = false;
  }
}
