import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PdftamponEnseigneComponent } from './pdftampon-enseigne.component';

describe('PdftamponEnseigneComponent', () => {
  let component: PdftamponEnseigneComponent;
  let fixture: ComponentFixture<PdftamponEnseigneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PdftamponEnseigneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PdftamponEnseigneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
