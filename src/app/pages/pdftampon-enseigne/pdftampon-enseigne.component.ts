import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { Response } from "../../models/response";
import { User } from "../../models/user";
import { MetiersService } from "../../services/metiers.service";
import { SendDatasService } from "../../services/send-datas.service";
import { UtilitiesService } from "../../services/utilities.service";

@Component({
  selector: "ngx-pdftampon-enseigne",
  templateUrl: "./pdftampon-enseigne.component.html",
  styleUrls: ["./pdftampon-enseigne.component.scss"],
})
export class PdftamponEnseigneComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private utilitiesService: UtilitiesService,
    private sendDatasService: SendDatasService,
    private metiersService: MetiersService
  ) {}

  // ************* begin declaration des variables  *************
  model = new User();
  //datasPointDeVente: any;
  datasPDFTampon: any;
  programmeFideliteTamponId: any;
  enseigneId: any;
  optionAffichage = environment.ACCUEIL;
  allPDVPourCePDFTampon = environment.allPDVPourCePDFTampon;
  //datasInformationRegleSecuriteTampon: any; // pr la liste des regles de securite
  isAdminEnseigne: boolean;
  datasUserEnDelit;
  // ************* end declaration des variables    *************

  ngOnInit() {
    this.isAdminEnseigne = this.utilitiesService.valueIsAdminEnseigne;
    this.utilitiesService.begin("ngOnInit");
    this.programmeFideliteTamponId = this.route.snapshot.params[environment.id];
    this.enseigneId = this.utilitiesService.enseigneId;
    if (this.programmeFideliteTamponId && this.enseigneId) {
      this.optionAffichage = environment.VOIR;

      // different de null
      // recuperer l'enseigne en question
      // récupération de la liste des roles
      let data = new User();
      data.id = this.programmeFideliteTamponId;
      data.enseigneId = this.enseigneId;
      // TODO:  se rassurer que le user est un admin
      this.metiersService
        .getByCriteria(environment.programmeFideliteTampon, data)
        .subscribe(
          (res) => {
            let response = new Response<any>();
            response = res;
            if (response != null && !response.hasError) {
              this.model = response.items[0];
            }
          },
          (err) => {
            console.log(err);
          }
        );
      this.getUsersEnDelit(data.id);
    } else {
      if (this.enseigneId) {
        // recuperer l'enseigne en question
        // récupération de la liste des roles
        let data = new User();
        if (this.isAdminEnseigne) {
          //data.id = this.programmeFideliteTamponId;
          data.enseigneId = this.enseigneId;
          // TODO:  se rassurer que le user est un admin
          this.metiersService
            .getByCriteria(environment.programmeFideliteTampon, data)
            .subscribe(
              (res) => {
                let response = new Response<any>();
                response = res;
                if (response != null && !response.hasError) {
                  this.datasPDFTampon = response.items;
                }
              },
              (err) => {
                console.log(err);
              }
            );
        } else {
          //data.id = this.programmeFideliteTamponId;
          data.userPDVId = this.utilitiesService.userId;
          // TODO:  se rassurer que le user est un admin
          this.metiersService
            .getByCriteriaCustom(
              environment.pointDeVenteProgrammeFideliteTampon,
              data
            )
            .subscribe(
              (res) => {
                let response = new Response<any>();
                response = res;
                if (response != null && !response.hasError) {
                  this.datasPDFTampon = response.items;
                }
              },
              (err) => {
                console.log(err);
              }
            );
        }
      } else {
        // merci de vous connectez
        this.utilitiesService.showToast(
          environment.merciDeVousConnectez,
          this.utilitiesService.statusInfo
        );
        // redirection
        this.router.navigateByUrl(environment.pageAccueil);
      }
    }
  }

  showPDFTampon(data) {
    this.getUsersEnDelit(data.id);

    this.router.navigateByUrl(
      environment.pages + environment.CPDFTamponEnseigne + "/" + data.id
    );
  }
  updatePDFTampon(data) {
    this.router.navigateByUrl(
      environment.pages + environment.CEditerPDFTamponEnseigne + "/" + data.id
    );
  }
  createPDFTampon(data) {
    this.router.navigateByUrl(
      environment.pages + environment.CEditerPDFTamponEnseigne
    );
  }
  deletePDFTampon(data) {
    this.utilitiesService.begin("deletePDFTampon");

    // popup

    // delete
    this.metiersService
      .delete(environment.programmeFideliteTampon, [data])
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          if (response != null && !response.hasError) {
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusSuccess
            );
            this.ngOnInit();
            this.router.navigateByUrl(
              environment.pages + environment.CPDFTamponEnseigne
            );
          } else {
            this.utilitiesService.showToast(
              response.status.message,
              this.utilitiesService.statusDanger
            );
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );
  }

  scannerManuellemnt(data) {
    this.sendDatasService.data = data;
    this.sendDatasService.isPDFTampon = true;
    this.router.navigateByUrl(
      environment.pages + environment.scannerManuellemntParProgramme
    );
  }

  getUsersEnDelit(programmeFideliteTamponId) {
    this.utilitiesService.begin("getUserEnDelit");
    let data = new User();

    data.programmeFideliteTamponId = programmeFideliteTamponId;
    if (!this.utilitiesService.valueIsAdminEnseigne) {
      data.pointDeVenteId = this.utilitiesService.pointDeVenteId;
    }

    this.metiersService
      .getByCriteriaCustom(
        environment.delitProgrammeFidelite,
        data
        //[dataForCarte]
      )
      //.getByCriteria(environment.user, data)
      .subscribe(
        (res) => {
          let response = new Response<any>();
          response = res;
          console.log("response datasUserEnDelit : ", response);
          if (response != null && !response.hasError) {
            this.datasUserEnDelit = response.items;
            console.log("this.datasUserEnDelit  ::: ", this.datasUserEnDelit);
            //this.dataGpsPointDeVente = this.model.dataGpsPointDeVente;
          }
        },
        (err) => {
          console.log(err);
          this.utilitiesService.showToast(
            environment.erreurDeConnexion,
            this.utilitiesService.statusInfo
          );
        }
      );
  }

  showUserEnDelit(dataUser) {
    this.utilitiesService.begin("showUserEnDelit");
    this.sendDatasService.data = dataUser;
    this.sendDatasService.data.pdfId = this.model.id;
    this.sendDatasService.isPDFTampon = true;
    this.router.navigateByUrl(environment.pages + environment.CDetailsClients);
  }
}
