import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SouscripitionEnseigneComponent } from './souscripition-enseigne.component';

describe('SouscripitionEnseigneComponent', () => {
  let component: SouscripitionEnseigneComponent;
  let fixture: ComponentFixture<SouscripitionEnseigneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SouscripitionEnseigneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SouscripitionEnseigneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
