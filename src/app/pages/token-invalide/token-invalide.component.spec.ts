import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TokenInvalideComponent } from './token-invalide.component';

describe('TokenInvalideComponent', () => {
  let component: TokenInvalideComponent;
  let fixture: ComponentFixture<TokenInvalideComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TokenInvalideComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TokenInvalideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
