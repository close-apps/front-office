import { NgModule } from "@angular/core";
import { AgmCoreModule } from "@agm/core";
import { LeafletModule } from "@asymmetrik/ngx-leaflet";
import { NgxEchartsModule } from "ngx-echarts";
import { NbCardModule } from "@nebular/theme";

import { ThemeModule } from "../../@theme/theme.module";
import { MapsRoutingModule, routedComponents } from "./maps-routing.module";

@NgModule({
  imports: [
    ThemeModule,
    AgmCoreModule.forRoot({
      //apiKey: "AIzaSyCpVhQiwAllg1RAFaxMWSpQruuGARy0Y1k",
      apiKey:"AIzaSyAD6jRBM-AFDDyVuF7vVNp7c_kBbehfqNo",
      libraries: ["places"],
    }),
    LeafletModule.forRoot(),
    MapsRoutingModule,
    NgxEchartsModule,
    NbCardModule,
  ],
  exports: [...routedComponents], // tres important pour utiliser les composants d'un module externe
  declarations: [...routedComponents],
})
export class MapsModule {}
