import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditerPdftamponEnseigneComponent } from './editer-pdftampon-enseigne.component';

describe('EditerPdftamponEnseigneComponent', () => {
  let component: EditerPdftamponEnseigneComponent;
  let fixture: ComponentFixture<EditerPdftamponEnseigneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditerPdftamponEnseigneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditerPdftamponEnseigneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
