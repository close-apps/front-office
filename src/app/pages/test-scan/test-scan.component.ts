// import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
// import { Appointment } from "../../models/appointment.model";
// import { OperationResponse } from "../../models/response.scan.model";

// @Component({
//   selector: "ngx-test-scan",
//   templateUrl: "./test-scan.component.html",
//   styleUrls: ["./test-scan.component.scss"],
// })
// export class TestScanComponent implements OnInit {
//   private scannerEnabled: boolean = true;
//   private transports: Transport[] = [];
//   private information: string =
//     "No se ha detectado información de ningún código. Acerque un código QR para escanear.";

//   constructor(
//     //private logService: LogService,
//     private cd: ChangeDetectorRef
//   ) {}

//   ngOnInit() {}

//   public scanSuccessHandler($event: any) {
//     this.scannerEnabled = false;
//     this.information = "Espera recuperando información... ";

//     const appointment = new Appointment($event);
//     console.log("$event: ", $event);
//     console.log("appointment: ", appointment.identifier);

//     /*
//     this.logService.logAppointment(appointment).subscribe(
//       (result: OperationResponse) => {
//         this.information = $event;
//         this.transports = result.object;
//         this.cd.markForCheck();
//       },
//       (error: any) => {
//         this.information =
//           "Ha ocurrido un error por favor intentalo nuevamente ... ";
//         this.cd.markForCheck();
//       }
//     );
//     */
//   }

//   public enableScanner() {
//     this.scannerEnabled = !this.scannerEnabled;
//     this.information =
//       "No se ha detectado información de ningún código. Acerque un código QR para escanear.";
//   }
// }

// interface Transport {
//   plates: string;
//   slot: Slot;
// }

// interface Slot {
//   name: string;
//   description: string;
// }
